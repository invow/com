import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from '../../authentication/authentication.service';
import { I18nService } from '../../i18n.service';
import { SharedService } from '../../../shared/services/shared.service';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menuHidden = true;
  areVisible: boolean;
  q: string;
  backgroundColor: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private authenticationService: AuthenticationService,
              private i18nService: I18nService,
              private fb: FormBuilder,
              public sharedService: SharedService
            ) {
                this.backgroundColor = this.sharedService.getHeaderColor();
                  this.route.queryParams.subscribe((params: Response) => {
                      this.q = params['q'] ? params['q'] : '';
                      if(this.q.indexOf('/') !== -1) {
                          switch(this.q) {
                              case this.q:
                              this.router.navigate(['/' + this.q], { replaceUrl: true });
                          }
                      }
                  });
                  this.drawLanguageToggle();
              }

  ngOnInit() {
      this.drawLanguageToggle();
  }

  drawLanguageToggle() {
      // Language Dropdown
      //------------------------------------------------------------------------------
      var langSwitcher = $('.lang-switcher'),
              langToggle = $('.lang-toggle');
      langToggle.on('click', function() {
          $(this).parent().toggleClass('open');
      });

      langSwitcher.on('click', function(e: any) {
      e.stopPropagation();
      });
      $(document).on('click', function(e: any) {
          langSwitcher.removeClass('open');
      });
  }

  toggleMenu() {
    this.menuHidden = !this.menuHidden;
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  logout() {
    this.authenticationService.logout()
      .subscribe(() => this.router.navigate(['/sign'], { replaceUrl: true }));
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  get username(): string {
    const credentials = this.authenticationService.credentials;
    return credentials ? credentials.username : null;
  }

}
