import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';


export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
  picture: string;
}

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

const credentialsKey = 'credentials';

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable()
export class AuthenticationService {

  private _credentials: Credentials;

  constructor(private http: Http) {
    this._credentials = JSON.parse(sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey));
  }

  authCall(context: LoginContext): Observable<string> {
      let data = "This is data.";
      let username: string = context.username;
      let password: string = context.password;
      let headers: Headers = new Headers();
      headers.append("Authorization", "Basic " + btoa(username + ":" + password));
      return this.http.post('/auth', data, { headers: headers, params: { access_token: 'I5qPiJkedAf5qFOWVcNqcUYJViaI4CaI'} })
            .map((res: Response) => res.json())
            .catch(() => Observable.of(':: ERROR ::'));
  }

  /**
   * Authenticates the user.
   * @param {LoginContext} context The login parameters.
   * @return {Observable<string>} The user credentials.
   */
  login(context: LoginContext){
      let username: string = context.username;
      let password: string = context.password;
      let headers: Headers = new Headers();
      console.log('HERE ::: ')

      console.log('USERNAME ::: ', username)
      console.log('PASSWORD ::: ', password)



      headers.append("Authorization", "Basic " + btoa(username + ":" + password));

      console.log('HEADERS ::: ', headers)

      return this.http.post('/auth', '', { headers: headers })
            .map((res: any) => {
                console.log('or HERE ::: ')

                let body = JSON.parse(res._body);
                const data = {
                    username: body.user.name,
                    token: body.token,
                    picture: body.user.picture
                };
                if (data.username && data.token) {
                    this.setCredentials(data, true);
                    Observable.of(data);
                }
            })
            //.catch(() => Observable.of(':: ERROR ::'));


    //   let Ok = false;
    //   const data = {
    //     username: 'not',
    //     token: 'not'
    //   };
     //
    //   login(username: string, password: string) {
    //         return this.http.post<any>('/api/authenticate', { username: username, password: password })
    //             .map(user => {
    //                 // login successful if there's a jwt token in the response
    //                 if (user && user.token) {
    //                     // store user details and jwt token in local storage to keep user logged in between page refreshes
    //                     localStorage.setItem('currentUser', JSON.stringify(user));
    //                 }
     //
    //                 return user;
    //             });
    //     }
    //  this.authCall(context)
    //   .subscribe((res: string) => {
    //           if(res !== ':: ERROR ::') {
    //               let response: string = JSON.stringify(res);
    //               let body = JSON.parse(response);
    //               const data = {
    //                   username: body.user.name,
    //                   token: body.token
    //               };
    //               this.setCredentials(data, context.remember);
    //           } else {
    //               this.logout();
    //           }
    //  });
    //  return Observable.of(data);
  }

  /**
   * Logs out the user and clear credentials.
   * @return {Observable<boolean>} True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.setCredentials();
    return Observable.of(true);
  }

  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  /**
   * Gets the user credentials.
   * @return {Credentials} The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials {
    return this._credentials;
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param {Credentials=} credentials The user credentials.
   * @param {boolean=} remember True to remember credentials across sessions.
   */
  private setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem(credentialsKey);
    }
  }

}
