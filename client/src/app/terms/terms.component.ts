import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {
    type: string;
    crowdlending: boolean;
    crowdfunding: boolean;
    equity: boolean;
    challenge: boolean;
    campaign: boolean;
    round: boolean;

  constructor(
      private router: Router,
      private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

}
