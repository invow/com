import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CartRoutingModule
  ],
  declarations: [
    CartComponent
  ]
})
export class CartModule { }
