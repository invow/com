import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { CreateComponent } from './create.component';
import { OptionsComponent } from './forms/options/options.component';
import { ChallengeComponent } from './forms/challenge/challenge.component';
import { CampaignComponent } from './forms/campaign/campaign.component';
import { RoundComponent } from './forms/round/round.component';
import { ImpulseComponent } from './forms/impulse/impulse.component';
import { PaymentsComponent } from './forms/payments/payments.component';
import { AuthenticationGuard } from '../core/authentication/authentication.guard';

const routes: Routes = Route.withShell([
  {
      path: 'create',
      component: CreateComponent,
      children: [
          {
              path: 'impulse',
              component: ImpulseComponent,
              data: { title: extract('+Adventure') }
          },
          {
              path: 'options',
              component: OptionsComponent
          },
          {
              path: 'challenge',
              component: ChallengeComponent
          },
          {
              path: 'campaign',
              component: CampaignComponent
          },
          {
              path: 'round',
              component: RoundComponent
          },
          {
              path: 'payments',
              component: PaymentsComponent
          }

      ],
      canActivate: [AuthenticationGuard],
      data: { title: extract('Create') }
  }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class CreateRoutingModule { }
