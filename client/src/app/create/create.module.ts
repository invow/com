import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { CreateService } from './create.service';
import { AdventureService } from '../shared/adventure.service';
import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './create.component';
import { ImpulseComponent } from './forms/impulse/impulse.component';
import { OptionsComponent } from './forms/options/options.component';
import { ChallengeComponent } from './forms/challenge/challenge.component';
import { CampaignComponent } from './forms/campaign/campaign.component';
import { RoundComponent } from './forms/round/round.component';
import { PaymentsComponent } from './forms/payments/payments.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { NgProgressModule } from 'ngx-progressbar';

import { SharedModule } from '../shared/shared.module';
import { SharedService } from '../shared/services/shared.service';
import { FormsService } from '../shared/forms/forms.service';

import { HttpClientModule } from '@angular/common/http';
import { NgxEditorModule } from 'ngx-editor';
import { PopoverModule } from 'ngx-bootstrap/popover';

@NgModule({
  imports: [
    BrowserModule,
    PopoverModule.forRoot(),
    HttpClientModule,
    NgxEditorModule,
    CommonModule,
    SharedModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    CreateRoutingModule,
    NgProgressModule
  ],
  declarations: [
    CreateComponent,
    ImpulseComponent,
    OptionsComponent,
    ChallengeComponent,
    CampaignComponent,
    RoundComponent,
    PaymentsComponent,
    BreadcrumbsComponent
],
providers: [
    CreateService,
    AdventureService,
    SharedService,
    FormsService
]
})
export class CreateModule { }
