import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateService } from '../../create.service';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {
    type: string;
    stepOne: FormGroup;
        collaborators: any = [];
        imgUploaded: boolean;
        imgRequired: boolean;
        imgExtension: boolean;
        imgSize: boolean;
        imgState: string;

        videoUploaded: boolean;
        videoRequired: boolean;
        videoExtension: boolean;
        videoSize: boolean;
        videoState: string;

        uploadingState: string;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private createService: CreateService
  ) {
      this.createForm();
  }

  ngOnInit() {
      this.imgUploaded = false;
      this.imgExtension = false;
      this.imgRequired = false;
      this.imgState = 'Upload image';

      this.videoUploaded = false;
      this.videoExtension = false;
      this.videoRequired = false;
      this.videoState = 'Upload video';

      this.uploadingState = 'Uploading';
      this.collaborators = this.stepOne.get('collaborators') as FormArray;
  }

  createForm() {
      this.stepOne = this.fb.group({
        step: ['1', Validators.required ],
        type: ['campaign', Validators.required ],
        title: ['',
        Validators.compose([
            Validators.required,
            Validators.minLength(3)
        ])],
        imageInput: ['',
          Validators.required
        ],
        videoInput: ['',
          Validators.required
        ],
        description: ['',
        Validators.compose([
            Validators.required,
            Validators.minLength(12),
            Validators.maxLength(16000)
        ])],
        category: ['', Validators.required],
        location: ['',
        Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(120)
        ])],
        days: ['',
        Validators.compose([
            Validators.required,
            Validators.min(1),
            Validators.max(90),
            Validators.pattern(/^\d+$/)
        ])],
        collaborators: this.fb.array([ this.createCollaborator() ])
      });
  }

  createCollaborator(): FormGroup {
      return this.fb.group({
          email: ['',
            Validators.compose([
                Validators.required,
                Validators.email
            ])
          ]
      });
  }

  addCollaborator(): void {
    this.collaborators = this.stepOne.get('collaborators') as FormArray;
    this.collaborators.push(this.createCollaborator());
  }

  deleteCollaborator() {
      if((<FormArray>this.stepOne.controls['collaborators']).controls.length !== 1) {
          (<FormArray>this.stepOne.controls['collaborators']).controls.splice(-1, 1);
          this.stepOne.value.collaborators.splice(-1, 1);
      }
  }

  validateFile(fileType: string, name: String, size: number, maxSize: number) {
    let ext = name.substring(name.lastIndexOf('.') + 1);
    let extLower = ext.toLowerCase();
    if(fileType === 'image') {
        this.imgExtension = (
            extLower === 'jpg' ||
            extLower === 'png' ||
            extLower === 'jpeg' ||
            extLower === 'bmp'
        );
        this.imgSize = (
            size <= maxSize
        );
        return (this.imgExtension && this.imgSize) ? true : false;
    }
    if(fileType === 'video') {
        this.videoExtension = (
            extLower === 'mp4' ||
            extLower === 'mkv' ||
            extLower === 'm4v' ||
            extLower === 'webm'
        );
        this.videoSize = (
            size <= maxSize
        );
        return (this.videoExtension && this.videoSize) ? true : false;
    }
  }

  onFileImgChange($event: any) {
      let file = $event.target.files[0]; // <--- File Object for future use.
      this.imgRequired = this.stepOne.get('imageInput').errors.required;

      if(
          $event.target.files &&
          file &&
          this.validateFile('image', file.name, file.size, 50000000)
      ) {
          this.imgState = this.uploadingState;
          this.stepOne.controls['imageInput'].setValue(file ? file.name : '');
          this.createService.postFile(file, 'image')
            .subscribe(
                res => {
                    this.imgState = file.name;
                    this.imgUploaded = true;
                },
                err => {
             this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                });
      }
  }

  onFileVideoChange($event: any) {
     let file = $event.target.files[0]; // <--- File Object for future use.
     this.videoRequired = this.stepOne.get('videoInput').errors.required;

     if(
         $event.target.files && file &&
         file &&
         this.validateFile('video', file.name, file.size, 5000000000)
     ) {
         this.videoState = this.uploadingState;
         this.stepOne.controls['videoInput'].setValue(file ? file.name : '');
         this.createService.postFile(file, 'video')
         .subscribe((res: boolean) => {
             if(res && file) {
                 this.videoState = file.name;
                 this.videoUploaded = true;
             }
          });
     }
  }

  get title() { return this.stepOne.get('title'); }
  get imageInput() { return this.stepOne.get('imageInput'); }
  get videoInput() { return this.stepOne.get('videoInput'); }
  get description() { return this.stepOne.get('description'); }
  get category() { return this.stepOne.get('category'); }
  get location() { return this.stepOne.get('location'); }
  get days() { return this.stepOne.get('days'); }
  get emailCollaborators() { return this.stepOne.get('collaborators'); }

  sendStepOneService() {
      this.stepOne.value.collaborators =
        JSON.stringify(this.stepOne.value.collaborators);
      this.createService.postStepOne(this.stepOne.value)
      .subscribe(
          res => {
              sessionStorage.setItem('adventure', res.id);
              console.log('Post Step One:');
              this.nextStep();
          },
          err => {
       this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
          });
  }

  public findInvalidControls(form: FormGroup) {
      const invalid = [];
      const controls = form.controls;
      for (const name in controls) {
          if (controls[name].invalid) {
              invalid.push(name);
          }
      }
      return invalid;
  }

  nextStep() {
  }

  sendStepOne() {
      let invalidCtrls: string[] = this.findInvalidControls(this.stepOne);
      if(invalidCtrls !== []) {
            Object.keys(this.stepOne.controls).forEach(field => {
              const control = this.stepOne.get(field);
              control.markAsTouched({ onlySelf: true });
            });
            if(invalidCtrls.indexOf('imageInput') !== -1) {
                this.imgRequired = true;
            }
            if(invalidCtrls.indexOf('videoInput') !== -1) {
                this.videoRequired = true;
            }
            if(
                this.imgState === this.uploadingState ||
                this.videoState === this.uploadingState
            ) {
                alert('Some files still uploading... Please wait a minute');
            } else {
                this.sendStepOneService();
            }
            window.scrollTo(0, 0);
      }
  }

}
