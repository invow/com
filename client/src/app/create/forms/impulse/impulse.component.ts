import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationStart, NavigationEnd, NavigationError  } from '@angular/router';
import { CreateService } from '../../create.service';
import { NgProgress } from 'ngx-progressbar';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';

import { SharedService } from '../../../shared/services/shared.service';
import { FormsService } from '../../../shared/forms/forms.service';
import { AuthenticationService } from '../../../core/authentication/authentication.service';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-impulse',
  templateUrl: './impulse.component.html',
  styleUrls: ['./impulse.component.scss']
})
export class ImpulseComponent implements OnInit {
    isLoading: boolean;
    imageName: string;
    imageId: string;
    type: string;
    areVisible: boolean;
    stepOne: FormGroup;
        collaborators: any = [];
        imgUploaded: boolean;
        imgRequired: boolean;
        imgExtension: boolean;
        imgSize: boolean;
        imgState: string;

        videoUploaded: boolean;
        videoRequired: boolean;
        videoExtension: boolean;
        videoSize: boolean;
        videoState: string;

        uploadingState: string;
    htmlContent: any;
    editorConfig: any;
    constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private createService: CreateService,
      public ngProgress: NgProgress,
      private sharedService: SharedService,
      private formsService: FormsService,
      private authenticationService: AuthenticationService
    ) {
      window.scrollTo(0, 0);
      this.editorConfig = {
        editable: true,
        spellcheck: false,
        height: '24rem',
        minHeight: '5rem',
        placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
        translate: 'no',
        imageEndPoint: `http://localhost:9000/api/files?access_token=${this.authenticationService.credentials.token}`,
      };
      router.events.subscribe( (event: Event) => {
          if (event instanceof NavigationStart) {
              // Show loading indicator
          }

          if (event instanceof NavigationEnd) {
            if(document.location.href.indexOf('/create/impulse') === -1) {
                // Hide loading indicator
                if(!sessionStorage.getItem('adventure')) {
                  //remove image from server
                  if(this.imageId){
                    this.createService.deleteFile(this.imageId, this.imageName)
                    .subscribe(
                      (res: any) => {
                        console.log(res);
                      },
                      (err: any) => {

                      }
                    )
                  }
                }

            }
          }
          if (event instanceof NavigationError) {
              // Hide loading indicator
              // Present error to user
              console.log(event.error);
          }
      });
      this.createForm();
  }

  ngOnInit() {
      this.isLoading = false;
      this.imgUploaded = false;
      this.imgExtension = false;
      this.imgRequired = false;
      this.imgState = 'Subir imagen';

      this.videoUploaded = false;
      this.videoExtension = false;
      this.videoRequired = false;
      this.videoState = 'Upload a video file';

      this.uploadingState = 'Subiendo...';
      this.collaborators = this.stepOne.get('collaborators') as FormArray;
  }

  createForm() {
      this.stepOne = this.formsService.createImpulseForm();
  }



  addCollaborator(): void {
    this.collaborators = this.stepOne.get('collaborators') as FormArray;
    this.collaborators.push(this.formsService.createCollaborator());
  }

  deleteCollaborator() {
      if((<FormArray>this.stepOne.controls['collaborators']).controls.length !== 1) {
          (<FormArray>this.stepOne.controls['collaborators']).controls.splice(-1, 1);
          this.stepOne.value.collaborators.splice(-1, 1);
      }
  }

  validateFile(fileType: string, name: String, size: number, maxSize: number) {
    let ext = name.substring(name.lastIndexOf('.') + 1);
    let extLower = ext.toLowerCase();
    if(fileType === 'image') {
        this.imgExtension = (
            extLower === 'jpg' ||
            extLower === 'png' ||
            extLower === 'jpeg' ||
            extLower === 'bmp'
        );
        this.imgSize = (
            size <= maxSize
        );
        return (this.imgExtension && this.imgSize) ? true : false;
    }
    if(fileType === 'video') {
        this.videoExtension = (
            extLower === 'mp4' ||
            extLower === 'mkv' ||
            extLower === 'm4v' ||
            extLower === 'webm'||
            extLower === 'ogv'
        );
        this.videoSize = (
            size <= maxSize
        );
        return (this.videoExtension && this.videoSize) ? true : false;
    }
  }

  onFileImgChange($event: any) {
      let file = $event.target.files[0]; // <--- File Object for future use.
      this.imgRequired = this.stepOne.get('imageInput').errors.required;

      if(
          $event.target.files &&
          file &&
          this.validateFile('image', file.name, file.size, 50000000)
      ) {
          this.imgState = this.uploadingState;
          this.stepOne.controls['imageInput'].setValue(file ? file.name : '');

          this.createService.postFile(file, 'image')
            .subscribe(
                  res => {
                    this.createService.postFileData(
                      file.name,
                      res ? res.name : 'Error while tried to find the name generated in adventure/uploads.',
                      'image'
                    )
                      .subscribe(
                        res => {
                          console.log('Success!');
                          console.log(res);
                          this.imageId = res.id;
                          this.imageName = res.path;
                        },
                        err => {
                     this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                        }
                    )
                    this.imgState = file.name;
                    this.stepOne.controls['img'].setValue(res ? res.name : '');
                    this.stepOne.controls['thumbnail'].setValue(res ? res.thumbnail : '');
                    this.imgUploaded = true;
                },
                err => {
             this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                });
      }
  }

  onFileVideoChange($event: any) {
     let file = $event.target.files[0]; // <--- File Object for future use.
     this.videoRequired = this.stepOne.get('videoInput').errors.required;

     if(
         $event.target.files && file &&
         file &&
         this.validateFile('video', file.name, file.size, 5000000000)
     ) {
         this.videoState = this.uploadingState;
         this.stepOne.controls['videoInput'].setValue(file ? file.name : '');

         this.sharedService.hideToolbar();
         this.ngProgress.start();
         this.createService.postFile(file, 'video')
          .subscribe(
              res => {
                  this.ngProgress.done();
                  this.sharedService.showToolbar();
                  this.videoState = file.name;
                  this.stepOne.controls['video'].setValue(res ? res.name : '');
                  this.videoUploaded = true;
              },
              err => {
           this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
              });
     }
  }

  get title() { return this.stepOne.get('title'); }
  get phone() { return this.stepOne.get('phone'); }
  get imageInput() { return this.stepOne.get('imageInput'); }
  get videoInput() { return this.stepOne.get('videoInput'); }
  get videoLink() { return this.stepOne.get('videoLink'); }
  get description() { return this.stepOne.get('description'); }
  get blurb() { return this.stepOne.get('blurb'); }
  get category() { return this.stepOne.get('category'); }
  get location() { return this.stepOne.get('location'); }
  //get days() { return this.stepOne.get('days'); }
  get goal() { return this.stepOne.get('goal'); }
  get emailCollaborators() { return this.stepOne.get('collaborators'); }

  sendStepOneService() {
      this.isLoading = true;
      this.stepOne.controls.ready.setValue(true);
      this.createService.postStepOne(this.stepOne.value)
      .subscribe(
          res => {
              sessionStorage.setItem('adventure', res.id);
              console.log('Post Step One:');
              //this.nextStep(res.invcode, res.slug);
               this.router.navigate(['/success'], { queryParams: { order: JSON.stringify({invcode: res.invcode, slug: res.slug}) }});;

          },
          err => {
           this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
          });
  }

  public findInvalidControls(form: FormGroup) {
      const invalid = [];
      const controls = form.controls;
      for (const name in controls) {
          if (controls[name].invalid) {
              invalid.push(name);
          }
      }
      return invalid;
  }

  nextStep(invcode: string, slug: string) {
      let url = '/adventure/' + invcode + '/' + slug
      this.router.navigate(['/search'], { queryParams: { q: url }});
  }

  sendStepOne() {
      let invalidCtrls: string[] = this.findInvalidControls(this.stepOne);
      if(invalidCtrls !== []) {
            Object.keys(this.stepOne.controls).forEach(field => {
              const control = this.stepOne.get(field);
              control.markAsTouched({ onlySelf: true });
            });
            if(invalidCtrls.indexOf('imageInput') !== -1) {
                this.imgRequired = true;
            }
            if(invalidCtrls.indexOf('videoInput') !== -1) {
                this.videoRequired = true;
            }
            if(
                this.imgState === this.uploadingState ||
                this.videoState === this.uploadingState
            ) {
                alert('Some files still uploading... Please wait a minute');
            } else {
                this.sendStepOneService();
            }
      }
  }
}
