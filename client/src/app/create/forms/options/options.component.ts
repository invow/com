import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdventureService } from '../../../shared/adventure.service';
import { FormsService } from '../../../shared/forms/forms.service';
import { CreateService } from '../../create.service';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {

    stepTwo: FormGroup;
        adventureId: string;
        credits: any = [];
        rewards: any = [];
        participations: any = [];

        fileUploaded: boolean;
        fileRequired: boolean;
        fileExtension: boolean;
        fileSize: boolean;
        fileState: string;

        uploadingState: string;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private createService: CreateService,
      private adventureService: AdventureService,
      private formsService: FormsService
  ) {
      if(sessionStorage.getItem('adventure')) {
          this.adventureId = sessionStorage.getItem('adventure');
          this.adventureService.getAdventure(this.adventureId)
          .subscribe(
              res => {
                  console.log(' --- GET ADVENTURE - RES --- ');
                  console.log(res);
                  console.log(' --- /GET ADVENTURE - RES --- ');

              },
              err => {
           this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
              }
          )
      } else {
          //this.router.navigate(['/search'], { queryParams: { q: '/create/impulse' }});
      }
      this.createForm();
  }

  ngOnInit() {
      this.credits = this.stepTwo.get('credits') as FormArray;

      this.fileUploaded = false;
      this.fileExtension = false;
      this.fileRequired = false;
      this.fileState = 'Upload documents!';

      this.uploadingState = 'Uploading';
  }

  createForm() {
      this.stepTwo = this.fb.group({
          step: [2, Validators.required ],
          credits: this.fb.array([]),
          rewards: this.fb.array([]),
          participations: this.fb.array([]),
          ready: [ false, Validators.required ]
      });
  }


  addCrowdlendingAgreement(): void {
    if(!(<FormArray>this.stepTwo.controls['credits'])) {
      this.stepTwo.addControl('credits', this.fb.array([]));
      this.credits = this.stepTwo.get('credits') as FormArray;
      this.credits.push(this.formsService.createCrowdlendingAgreement());
      this.stepTwo.get('credits')
        .get(String(0))
        .get('index')
        .setValue(this.credits.length);
    } else {
      this.credits = this.stepTwo.get('credits') as FormArray;
      this.credits.push(this.formsService.createCrowdlendingAgreement());
      this.stepTwo.get('credits')
        .get(String(this.credits.length - 1))
        .get('index')
        .setValue(this.credits.length);
    }
    var element = document.getElementById('crowdlending-' + String(this.credits.controls.length - 1));
    if(element) {
      element.scrollIntoView({ behavior: "smooth" });
      window.scrollTo(0, element.offsetTop + 480);
      alert("add three buttons at the bottom of the crowdlending selected!");
    } else { window.scrollTo(0, 240); }
  }

  addCrowdfundingAgreement(): void {
    if(!(<FormArray>this.stepTwo.controls['rewards'])) {
      this.stepTwo.addControl('rewards', this.fb.array([]));
      this.rewards = this.stepTwo.get('rewards') as FormArray;
      this.rewards.push(this.formsService.createCrowdfundingAgreement());
      this.stepTwo.get('rewards')
        .get(String(0))
        .get('index')
        .setValue(this.rewards.length);
    } else {
      this.rewards = this.stepTwo.get('rewards') as FormArray;
      this.rewards.push(this.formsService.createCrowdfundingAgreement());
      this.stepTwo.get('rewards')
        .get(String(this.rewards.length - 1))
        .get('index')
        .setValue(this.rewards.length);
    }
    var element = document.getElementById('crowdfunding-' + String(this.rewards.controls.length - 1));
    if(element) {
      element.scrollIntoView({ behavior: "smooth" });
      window.scrollTo(0, element.offsetTop + 360);
      alert("add three buttons at the bottom of the crowdfunding selected!");
    } else { window.scrollTo(0, 360); }// TO DO, Corregir la posicion si creo una participacion y desp un reward
  }

  addEquityAgreement(): void {
    if(!(<FormArray>this.stepTwo.controls['participations'])) {
      this.stepTwo.addControl('participations', this.fb.array([]));
      this.participations = this.stepTwo.get('participations') as FormArray;
      this.participations.push(this.formsService.createEquityAgreement());
      this.stepTwo.get('participations')
        .get(String(0))
        .get('index')
        .setValue(this.participations.length);
    } else {
      this.participations = this.stepTwo.get('participations') as FormArray;
      this.participations.push(this.formsService.createEquityAgreement());
      this.stepTwo.get('participations')
        .get(String(this.participations.length - 1))
        .get('index')
        .setValue(this.participations.length);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  deleteCrowdlendingAgreement() {
      if((<FormArray>this.stepTwo.controls['credits']).controls.length === 1) {
        (<FormArray>this.stepTwo.controls['credits']).controls.splice(-1, 1);
        (<FormGroup>this.stepTwo).removeControl('credits');
      } else {
        (<FormArray>this.stepTwo.controls['credits']).controls.splice(-1, 1);
        this.stepTwo.value.credits.splice(-1, 1);
      }
  }

  deleteCrowdfundingAgreement() {
      if((<FormArray>this.stepTwo.controls['rewards']).controls.length === 1) {
        (<FormArray>this.stepTwo.controls['rewards']).controls.splice(-1, 1);
        (<FormGroup>this.stepTwo).removeControl('rewards');
      } else {
        (<FormArray>this.stepTwo.controls['rewards']).controls.splice(-1, 1);
        this.stepTwo.value.rewards.splice(-1, 1);
      }
  }

  deleteEquityAgreement() {
    if((<FormArray>this.stepTwo.controls['participations']).controls.length === 1) {
      (<FormArray>this.stepTwo.controls['participations']).controls.splice(-1, 1);
      (<FormGroup>this.stepTwo).removeControl('participations');
    } else {
      (<FormArray>this.stepTwo.controls['participations']).controls.splice(-1, 1);
      this.stepTwo.value.participations.splice(-1, 1);
    }
  }

  validateFile(fileType: string, name: String, size: number, maxSize: number) {
    let ext = name.substring(name.lastIndexOf('.') + 1);
    let extLower = ext.toLowerCase();
    if(fileType === 'doc') {
        this.fileExtension = (
            extLower === 'docx' ||
            extLower === 'pptx' ||
            extLower === 'xlsx' ||
            extLower === 'pdf'
        );
        this.fileSize = (
            size <= maxSize
        );
        return (this.fileExtension && this.fileSize) ? true : false;
    }
  }

  onFileChange($event: any, i: number) {
      let file = $event.target.files[0]; // <--- File Object for future use.
      if(
          $event.target.files &&
          file &&
          this.validateFile('doc', file.name, file.size, 50000000)
      ) {
          this.fileState = this.uploadingState;
          this.stepTwo.get('participations').get(String(i)).get('fileInput').setValue(file ? file.name : '');
          this.createService.postFile(file, 'doc')
            .subscribe(
                res => {
                    this.createService.postFileData(
                      file.name,
                      res ? res.name : 'Error while tried to find the name generated in adventure/uploads.',
                      'doc'
                    )
                      .subscribe(
                        res => {
                          console.log('Success!');
                          console.log(res);

                        },
                        err => {
                          console.log('Error!');
                          console.log(err);
                        }
                    )
                    this.fileState = file.name;
                    this.stepTwo.get('participations').get(String(i)).get('doc').setValue(res ? res.name : '');
                    this.fileUploaded = true;
                },
                err => {
             this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                });
      }
  }

  get allcredits() { return this.stepTwo.get('credits'); }
  get allrewards() { return this.stepTwo.get('rewards'); }
  get allparticipations() { return this.stepTwo.get('participations'); }
  get fileInput() { return this.stepTwo.get('fileInput'); }
  get category() { return this.stepTwo.get('category'); }
  get goal() { return this.stepTwo.get('goal'); }

  public findInvalidControls(form: FormGroup) {
      const invalid = [];
      const controls = form.controls;
      for (const name in controls) {
          if (controls[name].invalid) {
              invalid.push(name);
          }
      }
      return invalid;
  }
  nextStep() {
      this.router.navigate(['/search'], { queryParams: { q: '/' }});
  }

  sendStepTwoService() {
      this.createService.putStepTwo(this.adventureId, this.stepTwo.value)
      .subscribe(
          res => {
              console.log('Post Step Two:');
              this.nextStep();
          },
          err => {
       this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
          });
  }

  sendStepTwo() {
      let invalidCtrls: string[] = this.findInvalidControls(this.stepTwo);
      if(this.stepTwo.invalid || invalidCtrls.length !== 0) {
          if(invalidCtrls.length !== 0) {
            Object.keys(this.stepTwo.controls).forEach(field => {
              const control = this.stepTwo.get(field);
              control.markAsTouched({ onlySelf: true });
            });
            if(invalidCtrls.indexOf('fileInput') !== -1) {
                this.fileRequired = true;
            }

            if(
                this.fileState === this.uploadingState
            ) {
                alert('Some files still uploading... Please wait a minute');
            }
          }
      } else {
          this.stepTwo.controls['ready'].setValue(true);
          this.sendStepTwoService();
      }
      window.scrollTo(0, 0);
  }
}
