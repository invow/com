import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateService } from '../../create.service';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
    datePattern = '/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/';

    stepThree: FormGroup;
        adventureId: string;
        imgExtension: boolean;
        imgSize: boolean;
        imgIdFrontUploaded: boolean;
        imgIdBackUploaded: boolean;
        imgIdFrontRequired: boolean;
        imgIdBackRequired: boolean;
        imgIdFrontState: string;
        imgIdBackState: string;
        bankAccounts: any = [];
        paymentSources: any = [];
        banksList: Array<Object>;
        paymentSourcesList: Array<Object>;

        bankAccountInputs: boolean;
        paymentSourceInputs: boolean;
        typeSelected: string;
        ppOrMp: string;
        code: string;
        type: string;

        advType: string;

        uploadingState: string;
        challengeOn: boolean;
        impulseOn: boolean;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private createService: CreateService
  ) {
      if(sessionStorage.getItem('adventure')) {
          this.adventureId = sessionStorage.getItem('adventure');
      } else {
          /*
          this.router.navigate([`/new`], {
              replaceUrl: true
          });
          */
      }
      this.createForm();
  }

  ngOnInit() {
      this.challengeOn = false;

      this.route.queryParams.subscribe((params: Response) => {
          let validator: boolean;
          if(params['t']) {
            this.challengeOn =
            (params['t'].indexOf('challenge') !== -1) ?
            true : false;

            this.impulseOn =
            (params['t'].indexOf('impulse') !== -1) ?
            true : false;

            validator = (
                this.challengeOn ||
                this.impulseOn
             );
            if(!validator) {
              this.router.navigate(
                  [`/new`], {
                    replaceUrl: true
                });
            }
          }
      }, (err: Response) => {
          console.log("err");
          if(err) {
              this.router.navigate(
                  [`/new`], {
                    replaceUrl: true
                });
          }
      });
      this.imgIdFrontUploaded = false;
      this.imgIdBackUploaded = false;
      this.imgExtension = false;
      this.imgIdFrontRequired = false;
      this.imgIdBackRequired = false;
      this.imgIdFrontState = 'Upload image';
      this.imgIdBackState = 'Upload image';

      this.uploadingState = 'Uploading';
      //this.bankAccounts = this.stepThree.get('bankAccounts') as FormArray;
      //this.paymentSources = this.stepThree.get('paymentSources') as FormArray;
      this.bankAccountInputs = false;
      this.paymentSourceInputs = false;

      this.typeSelected = 'paypal';
      this.ppOrMp = 'example@paypal.com';
      this.type = 'Type';

      this.banksList = this.createService.getBanks();
      this.paymentSourcesList = this.createService.getPaymentSources();
  }

  createForm() {
      this.stepThree = this.fb.group({
         adventure: [this.adventureId, Validators.required ],
         ready: [ false, Validators.required ],
         step: [3, Validators.required ],
         fullName: ['',
           Validators.compose([
               Validators.required,
               Validators.minLength(3),
               Validators.maxLength(30)
           ])
         ],
         birthday: [''//,
              //Validators.compose([
                  //Validators.required,
                  //Validators.pattern(this.datePattern)
              //])
        ],
        email: ['',
            Validators.compose([
                Validators.required,
                Validators.email
          ])
        ],
        phone: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(30),
                Validators.pattern(/^\d+$/)
          ])
        ],
        address: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(30),
            ])
        ],
        city: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(60)
            ])
        ],
        zip: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(30),
                Validators.pattern(/^\d+$/)
            ])
        ],
        country: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(60)
            ])
        ],
        identification: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(30),
                Validators.pattern(/^\d+$/)
              ])
        ],
        idFront: ['',
             Validators.compose([
                 Validators.required
             ])
       ],
       idBack: ['',
          Validators.compose([
              Validators.required
          ])
       ],
       accountSelected: ['',
            Validators.required
       ],
       paymentSourceSelected: ['',
          Validators.required
       ],
       bankAccounts: this.fb.array([]),
       paymentSources: this.fb.array([])
      });
  }

  createBankAccount(): FormGroup {
      return this.fb.group({
          ownerName: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(60)

            ])
      ],
          ownerId: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(30),
                Validators.pattern(/^\d+$/)
            ])
      ],
          accountNumber: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(16),
                Validators.maxLength(16),
                Validators.pattern(/^\d+$/)
            ])
      ]
      });
  }

  createPaymentSource(): FormGroup {
      return this.fb.group({
         accountEmail: ['',
            Validators.compose([
                Validators.required,
                Validators.email
            ])
     ],
         accountType: ['mercadopago',
            Validators.compose([
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(10)
            ])
    ]
      });
  }

  addBankAccount(): void {
    this.bankAccounts = this.stepThree.get('bankAccounts') as FormArray;
    this.bankAccounts.push(this.createBankAccount());
  }

  deleteBankAccount() {
      (<FormArray>this.stepThree.controls['bankAccounts']).controls.splice(-1, 1);
      //(<FormGroup>this.stepThree).removeControl('bankAccounts');
  }

  addPayment(): void {
    this.paymentSources = this.stepThree.get('paymentSources') as FormArray;
    this.paymentSources.push(this.createPaymentSource());
  }

  deletePayment() {
      (<FormArray>this.stepThree.controls['paymentSources']).controls.splice(-1, 1);
      //(<FormGroup>this.stepThree).removeControl('paymentSources');
  }

  get fullName() { return this.stepThree.get('fullName') }
  get birthday() { return this.stepThree.get('birthday') }
  get email() { return this.stepThree.get('email') }
  get phone() { return this.stepThree.get('phone') }
  get address() { return this.stepThree.get('address') }
  get city() { return this.stepThree.get('city') }
  get zip() { return this.stepThree.get('zip') }
  get country() { return this.stepThree.get('country') }
  get identification() { return this.stepThree.get('identification') }
  get idFront() { return this.stepThree.get('idFront') }
  get idBack() { return this.stepThree.get('idBack') }

  validateFile(fileType: string, name: String, size: number, maxSize: number) {
    let ext = name.substring(name.lastIndexOf('.') + 1);
    let extLower = ext.toLowerCase();
    if(fileType === 'image') {
        this.imgExtension = (
            extLower === 'jpg' ||
            extLower === 'png' ||
            extLower === 'jpeg' ||
            extLower === 'bmp'
        );
        this.imgSize = (
            size <= maxSize
        );
        return (this.imgExtension && this.imgSize) ? true : false;
    }
  }

  onFileIdFrontImgChange($event: any) {
      let file = $event.target.files[0]; // <--- File Object for future use.
      this.imgIdFrontRequired = this.stepThree.get('idFront').errors.required;

      if(
          $event.target.files &&
          file &&
          this.validateFile('image', file.name, file.size, 50000000)
      ) {
          this.imgIdFrontState = this.uploadingState;
          this.stepThree.controls['idFront'].setValue(file ? file.name : '');
          this.createService.postFile(file, 'image')
            .subscribe(
                res => {

                    this.imgIdFrontState = file.name;
                    this.imgIdFrontUploaded = true;
                },
                err => {
             this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                });
      }
  }

  onFileIdBackImgChange($event: any) {
      let file = $event.target.files[0]; // <--- File Object for future use.
      this.imgIdBackRequired = this.stepThree.get('idBack').errors.required;

      if(
          $event.target.files &&
          file &&
          this.validateFile('image', file.name, file.size, 50000000)
      ) {
          this.imgIdBackState = this.uploadingState;
          this.stepThree.controls['idBack'].setValue(file ? file.name : '');
          this.createService.postFile(file, 'image')
            .subscribe(
                res => {
                    this.imgIdBackState = file.name;
                    this.imgIdBackUploaded = true;
                },
                err => {
             this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                });
      }
  }

  newBankAccount() {
      this.bankAccountInputs = true;
      this.addBankAccount();
  }

  showAllAccounts() {
      this.bankAccountInputs = false;
      this.deleteBankAccount();
  }

  newPaymentSource() {
      this.paymentSourceInputs = true;
      this.addPayment();
  }

  showAllPaymentSources() {
      this.paymentSourceInputs = false;
      this.deletePayment();
  }

  typeSelect($event: any) {
      this.typeSelected = $event.target.value;
      this.paymentSourceInputs = true;

      switch(this.typeSelected) {
          case 'paypal':
          this.ppOrMp = 'example@paypal.com';
          this.type = 'Paypal';
          break;
          case 'mercadopago':
          this.ppOrMp = 'example@mercadopago.com';
          this.type = 'Mercadopago';
          break;
      }
  }

  bankAccountClicked($event: any) {
      this.stepThree.controls['accountSelected'].setValue($event.target.value);
  }

  paymentSourceClicked($event: any) {
      this.stepThree.controls['paymentSourceSelected'].setValue($event.target.value);
  }

  public findInvalidControls(form: FormGroup) {
      const invalid = [];
      const controls = form.controls;
      for (const name in controls) {
          if (controls[name].invalid) {
              invalid.push(name);
          }
      }
      return invalid;
  }

  sendBank() {
      this.createService.postBank(this.stepThree.controls['bankAccounts'].value[0])
      .subscribe((res: any) => {
          if(res) {
              if(res === ':: ERROR ::') {
                  console.log(res);
              }
          }
       });
  }

  sendSource() {
      this.createService.postSource(this.stepThree.controls['paymentSources'].value[0])
      .subscribe((res: any) => {
          if(res) {
              if(res === ':: ERROR ::') {
                  console.log(res);
              }
          }
       });
  }

  sendStepThree() {
      if(this.stepThree.controls.bankAccounts) {
          if((<FormArray>this.stepThree.controls['bankAccounts']).length === 0) {
              (<FormGroup>this.stepThree).removeControl('bankAccounts');
          }
      }
      if(this.stepThree.controls.paymentSources) {
          if((<FormArray>this.stepThree.controls['paymentSources']).length === 0) {
              (<FormGroup>this.stepThree).removeControl('paymentSources');
          }
      }
      this.stepThree.controls['ready'].setValue(true);
      this.createService.putStepThree(this.adventureId, this.stepThree.value)
      .subscribe((res: any) => {
          if(res) {
              if(res === ':: ERROR ::') {
                  console.log(res);
              }
              this.createService.postPayment(this.stepThree.value)
                .subscribe(
                  res => {
                    console.log(res)
                  },
                  err => {
               this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                  }
                )
                window.scrollTo(0, 0);
                this.router.navigate(['/adventure']);
          }
       });
  }


}
