import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})

export class BreadcrumbsComponent implements OnInit {
    @Input() states: string[] = null;
    @Input() initial: string;
    step: number;
    type: string;

  constructor(
      private router: Router,
      private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
  }
 }
