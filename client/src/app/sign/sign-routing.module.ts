import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { SignComponent } from './sign.component';

const routes: Routes = Route.withShell([
  { path: 'sign', component: SignComponent, data: { title: extract('Sign') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class SignRoutingModule { }
