import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SignRoutingModule } from './sign-routing.module';
import { SignComponent } from './sign.component';
import { SignService } from './sign.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    SignRoutingModule
  ],
  declarations: [
    SignComponent
  ],
providers: [
  SignService
]
})
export class SignModule { }
