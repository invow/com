import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { SuccessRoutingModule } from './success-routing.module';
import { SuccessComponent } from './success.component';
import { ChartsModule } from 'ng2-charts';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    SuccessRoutingModule,
    ChartsModule
  ],
  declarations: [
    SuccessComponent
  ]
})
export class SuccessModule { }
