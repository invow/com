import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Checkout } from '../shared/classes/checkout';
import { AccountService } from '../shared/services/account.service';
import { CartService } from '../shared/services/cart.service';
import { CountriesService } from '../shared/services/countries.service';
import { CheckoutService } from './checkout.service';
import { ProfileService } from '../profile/profile.service';
import { PriceService } from '../price/price.service';
import { CheckoutFormsService } from '../shared/forms/checkout.forms.service'


import { _throw } from 'rxjs/observable/throw';
import { FormGroup } from '@angular/forms';

declare var Mercadopago: any;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
    isLoading: boolean;
    checkoutForm: FormGroup;
    items : Array<Object> = [];
    checkout: Checkout;
    currencies : Array<Object> = [];
    user : any;
    fundArs: boolean;
    fundUsd: boolean;
    formError: boolean;
    payment : any = {
      email : "",
      cardNumber: "",
      securityCode: "",
      cardExpirationMonth: "",
      cardExpirationYear: "",
      cardholderName: "",
      docType: "",
      docNumber: "",
      payment_method_id: "",
      payment_type_id: "",
      token: ""
    }
    docTypes: Array<any> = [];
    countries: Array<any>;
    invowAccount: boolean = false;
    commission: number;
    commissionLabel: string;
    instant: boolean;
    instantItem: any;
    subTotals: any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        public cartService: CartService,
        public profileService: ProfileService,
        public accountService: AccountService,
        public checkoutService: CheckoutService,
        public countriesService: CountriesService,
        private priceService: PriceService,
        private checkoutFormService: CheckoutFormsService
    ) {
      this.checkoutForm = this.checkoutFormService.createCheckout();
    }

    ngOnInit() {
        var _t = this;
        this.isLoading = true;
        this.formError = false;
        this.checkout = new Checkout;
        Mercadopago.setPublishableKey("TEST-4cbd47a0-451b-4d8e-960e-8919b568e9ad");
        Mercadopago.getIdentificationTypes(function(status: any, response: any){
          _t.docTypes = response;
        })

        this.countriesService.all()
        .subscribe(countries => {
          this.countries = countries;
        });

        this.priceService.getPrices()
          .subscribe(
            res => {
              if(typeof res !== 'string') {
                const response: any = res;
                this.commission = response[6].value;
                this.commissionLabel = response[6].includes[1];
              }
            },
            err => {
              console.log(err);
            }
          )
        /**
         * @queryParams
         */
        this.route.queryParams.subscribe((params: Response) => {
          /**
           * @Instant
           */
          if(params['item']) {
            this.instant = true;
            this.instantItem = JSON.parse(params['item']);
          }
        });
        this.checkout.items = this.instant ? this.instantItem : this.cartService.get();
        
        this.accountService.account().subscribe((params: any) => {
          this.isLoading = false;
          this.subTotals = this.instant ? 
            [ { code: 'ARS', value: this.instantItem[0].price } ] :
            this.cartService.subtotals();

          let subtotals = this.subTotals;
          if(params && params !== [] && subtotals !== []) {
            params.forEach((item: any) => {
              subtotals.forEach((st: any) => {
                if((st.code == '' || st.code === 'ARS') && item.currency === 'ARS') {
                  this.fundArs = (item.amount >= st.value);
                }
                if(st.code === 'USD' && item.currency === 'USD') {
                  this.fundUsd = (item.amount >= st.value);
                }
              })
            })
          }
        });
        if(this.fundArs || this.fundUsd) {
          this.invowAccount = true;
        }
    }

    get address() { return this.checkoutForm.get('address'); }
    get phone() { return this.checkoutForm.get('phone'); }

    isValid() {
      return (
        this.payment.email &&
        this.payment.docType &&
        this.payment.docNumber &&
        this.payment.cardNumber &&
        this.payment.securityCode && 
        this.payment.cardExpirationMonth &&
        this.payment.cardExpirationYear &&
        this.payment.cardholderName
      );
    }

    onChange(deviceValue: string) {
      switch(deviceValue) {
        case 'mercadoPago':
        this.invowAccount = false;
        break;
        case 'invowAccount':
        this.invowAccount = true;
        break;
      }
    }

    isEmpty(obj: any) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
  }

    doCheckout(){
        this.isLoading = true;
        let valid = (
          !this.isEmpty(this.checkout.items) &&
          this.checkout.firstName &&
          this.checkout.firstName !== '' &&
          this.checkout.lastName &&
          this.checkout.lastName !== '' &&
          this.checkout.phone &&
          this.checkout.phone !== '' &&
          this.checkout.address &&
          this.checkout.address !== '' &&
          this.checkout.email &&
          this.checkout.email !== '' &&
          this.checkout.country &&
          this.checkout.country !== '' &&
          this.checkout.state &&
          this.checkout.state !== '' &&
          this.checkout.city &&
          this.checkout.city !== '' &&
          this.checkout.company &&
          this.checkout.company !== '' &&
          this.checkout.zip &&
          this.checkout.zip !== ''
        );

        if(valid) {
          this.checkoutService.checkout(this.checkout, this.phone.value, this.address.value).subscribe(
            (params: Response) => {
              if(!this.instant)
                this.cartService.clear();
              this.router.navigate(['/success'], { queryParams: { order: JSON.stringify(params) }});

            },
            (err: Response) => {
              this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});
            }
          );
        } else {
          this.isLoading = false;
          this.formError = true;
        }
    }
    doPay(){
      this.isLoading = true;
      var _t = this;
      var ccNumber = JSON.parse(JSON.stringify(this.payment.cardNumber))
      Mercadopago.getPaymentMethod({
        "bin": ccNumber.toString().replace(/[ .-]/g, '').slice(0, 6)
      }, function(status: any, response: any){
        _t.payment.payment_method_id = response[0].id;
        _t.payment.payment_type_id = response[0].payment_type_id;
        Mercadopago.createToken(_t.payment, function(status: any, response: any){
          _t.payment.token = response.id;
          _t.checkout.amount = "2";
          _t.checkout.payment = _t.payment;
          _t.checkoutService.checkout(_t.checkout, _t.phone.value, _t.address.value).subscribe(
            (params: Response) => {
              if(!_t.instant)
                _t.cartService.clear();
              _t.router.navigate([`/success`], { queryParams: { order: JSON.stringify(params)}, replaceUrl: true });
            },
            (err: any) => {
              _t.router.navigate([`/error`], { queryParams: { err: JSON.stringify(err)}, replaceUrl: true });
            }
          );
        })
      })
    }
}
