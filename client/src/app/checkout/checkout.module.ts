import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CheckoutService} from './checkout.service';
import { AccountService } from '../shared/services/account.service';
import { CountriesService } from '../shared/services/countries.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CheckoutRoutingModule } from './checkout-routing.module';
import { CheckoutComponent } from './checkout.component';
import { CheckoutFormsService } from '../shared/forms/checkout.forms.service'

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CheckoutRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    CheckoutComponent,
  ],
  providers: [
    CheckoutService,
    AccountService,
    CountriesService,
    CheckoutFormsService
  ]
})
export class CheckoutModule { }
