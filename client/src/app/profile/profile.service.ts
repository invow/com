import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

import { AuthenticationService } from '../core/authentication/authentication.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const routes = {
  quote: (c: RandomQuoteContext) => `/jokes/random?category=${c.category}`
};

export interface RandomQuoteContext {
  // The quote's category: 'nerdy', 'explicit'...
  category: string;
}

@Injectable()
export class ProfileService {

  constructor(
      private http: Http,
      private authenticationService: AuthenticationService
  ) {

  }

  getCurrentUser() {
    const token = this.authenticationService.credentials.token;
    let options       = new RequestOptions({
        params: { access_token: token }
    }); // Create a request option
    const endpoint = `/users/me`;
    return this.http.get(endpoint, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
  }

  getAccount(username: string) {
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = new RequestOptions({
            params: { access_token: token }
        }); // Create a request option
        const endpoint = `/profiles/username/${username}`;
        return this.http.get(endpoint, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }
  }

   postFile(fileToUpload: File, fileKey: string) {
        if(this.authenticationService.isAuthenticated()) {
            const token = this.authenticationService.credentials.token;
            //let headers      = new Headers({ 'inw-token': token }); // ... Set content type to JSON
            let options       = {
                //headers: headers,
                params: { access_token: token },
                reportProgress: true,
                observe: 'events'
            }; // Create a request option
            const endpoint = '/adventures/upload';
            const formData: FormData = new FormData();
            formData.append(fileKey, fileToUpload, fileToUpload.name);
            return this.http
              .post(endpoint, formData, options) //headers: yourHeadersConfig
              .map((res) => res.json()) //return true;
              .catch((e:string) => e);
        }
   }

   postFileData(name: string, path: string, type: string) {
     if(this.authenticationService.isAuthenticated()) {
         const token = this.authenticationService.credentials.token;
         let options       = new RequestOptions({
             params: { access_token: token }
         }); // Create a request option
         const endpoint = '/files';
         return this.http
           .post(endpoint, { name: name, path: path, type: type }, options)
           .map((res: any) => res.json())
           .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
     }
   }

   /* Step One */
   postStepOne(form: FormData) {
     if(this.authenticationService.isAuthenticated()) {
         const token = this.authenticationService.credentials.token;
         let options       = new RequestOptions({
             //headers: headers,
             params: { access_token: token }
         }); // Create a request option
         const endpoint = '/adventures';
         return this.http
           .post(endpoint, form, options)
           .map((res: any) => res.json())
           .catch((error:any) => {
             console.log('ERROR ::: ::: :::');
             console.log(error);
             return Observable.throw(error.json().error || 'Server error')
           }); //...errors if any
           //.catch((e:string) => e);
     }
  }
  putAccount(id: string, body: any) {
      if(this.authenticationService.isAuthenticated()) {
          const token = this.authenticationService.credentials.token;
          const endpoint = `/profiles/${id}`;
          let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
          let options = new RequestOptions({
              headers: headers,
              params: { access_token: token }
          });
          return this.http.put(endpoint, body, options) // ...using post request
            .map(() => { return true }) // ...and calling .json() on the response to return data
            .catch((error:any) => Observable.throw(body)); //...errors if any  error.json().error || body
      }
  }
  getOperationByInvop(invop: string) {
    const endpoint = `/operations/invop/${invop}` //&sort=${field} //q=${field}
    return this.http.get(endpoint)
      .map(res => res.json())
      .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
  }
}
