import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { AdventureService } from '../shared/adventure.service';
import { FormsService } from '../shared/forms/forms.service';
import { ProfileService } from './profile.service';
import { AccountService } from '../shared/services/account.service';
import { ProfileFormsService } from '../shared/forms/profile.forms.service';
import { CreateService } from '../create/create.service';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { NgxEditorModule } from 'ngx-editor';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { PipeModule } from '../shared/pipes/pipe.module'; // import our pipe here
import { AuthenticationService } from '../core/authentication/authentication.service';
import { ModalModule } from 'angular-custom-modal';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PopoverModule.forRoot(),
    HttpClientModule,
    NgxEditorModule,
    TranslateModule,
    PipeModule,
    ProfileRoutingModule,
    ModalModule
  ],
  declarations: [
    ProfileComponent
  ],
  providers: [
    AdventureService,
    FormsService,
    ProfileService,
    AccountService,
    ProfileFormsService,
    CreateService,
    AuthenticationService
  ]
})
export class ProfileModule { }
