import { Component, OnInit, isDevMode } from '@angular/core';
import { environment } from '../../environments/environment';
import  { environment  as environmentProd } from '../../environments/environment.prod';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { Logger } from '../core/logger.service';
import { AdventureService } from '../shared/adventure.service';
import { SharedService } from '../shared/services/shared.service';
import { SignService } from '../sign/sign.service';
import { UserFormsService } from '../shared/forms/user.forms.service';
import { SubscriptionFormsService } from '../shared/forms/subscription.forms.service';
import { AccountService } from '../shared/services/account.service'

const log = new Logger('Login');

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  quote: string;
  isLoading: boolean;
  subscriptionLoading: boolean;
  subscriptionFormView: boolean;
  subscriptionSuccess: boolean;
  newest: Array<any>;
  likeBorderIcon: boolean;
  likeIcon: boolean;
  env: string;
  verticals: any;
  subscriptionForm: FormGroup;
  motivationalMessage: boolean;
  adventureCapital: number;
  currentPercentage: number;
  styleExpression: string;
  loadingImages: boolean = true;
  isImgLoaded: boolean = false;


  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private adventureService: AdventureService,
      public sharedService: SharedService,
      private authenticationService: AuthenticationService,
      private signService: SignService,
      private userFormsService: UserFormsService,
      private subscriptionFormsService: SubscriptionFormsService,
      private accountService: AccountService
  ) {

  }

  ngOnInit() {
    this.isLoading = true;
    window.scrollTo(0, 0);

    const userAgent = navigator.userAgent;
    let isMobile = false;

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(userAgent))
      isMobile = true;
    
    if(isMobile)
      this.router.navigate([`/adventures`], { replaceUrl: true });

    if(isDevMode()) 
      this.env = environment.assetsUrl;
    else
      this.env = environmentProd.assetsUrl;

    this.subscriptionLoading = false;
    this.subscriptionFormView = true;
    this.subscriptionSuccess = false;
    this.motivationalMessage = false;

    this.adventureService.getNewest()
        .subscribe(
            res => {
                this.newest = res;
                this.newest.forEach((item, index) => {
                  this.AdventuresInitialPayload(item.id, index, item.goal);
                  if (!item.img.includes('assets/img/adv-img.png')) {
                    this.newest[index].img = `${this.env}/image/thumb/${item.img}`;
                  }
                  item.likes.forEach((subItem: any) => {
                    if(subItem.user === this.username) {
                      this.newest[index].liked = true;
                    }
                  });
                });
                this.isLoading = false;
            },
            err => {
              this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
            }
        )

      this.createForms();
  }

    goToAdventure(invcode: string, slug: string) {
      this.router.navigate([`/adventure/${invcode}/${slug}`], { replaceUrl: true });
    }

    createForms() {
      this.subscriptionForm = this.subscriptionFormsService.createSubscription();
    }

    /**
     * Like
     * @param advId Sting
     * @param likes *
     * @param index Number
     */
    like(advId: String, likes: any, index: number) {
      this.newest[index].liked = true;
      if(this.username) {
        this.adventureService.like(advId, likes, true)
        .subscribe(
          res => res,
          err => err
        );
      } else {
        this.router.navigate([`/sign`], { replaceUrl: true });
      }
    }

    /**
     * Unlike
     * @param advId String
     * @param likes Array<Sting>
     * @param index Number
     */
    unlike(advId: string, likes: Array<String>, index: number) {
      this.newest[index].liked = false;
      const updatedLikes = likes.splice(1, index);
      this.newest[index].likes = updatedLikes;

      likes.forEach((item: any, index: number) => {
        if(item.user === this.username) {
          this.adventureService.like(advId, updatedLikes, false)
          .subscribe(
            res => {
              this.newest.forEach((item, index) => {
                if(item.id === advId) {
                  this.newest[index].likes = updatedLikes;
                }
              });
            },
            err => err
          );
        }
      })
    }

    cancel(advId: string) {
      this.newest.forEach((item: any, index: number) => {
        if(item.id === advId) {
          this.newest.splice(index, 1);
          if(this.newest.length < 1) {
            this.motivationalMessage = true;
          }
        }
      });
    }

    saveSubscription() {
      this.subscriptionLoading = true;
      this.sharedService.postSubscription(this.subscriptionForm.value)
      .subscribe(res => {
        this.subscriptionSuccess = true;
        window.scrollTo(0, document.body.scrollHeight);
        setTimeout(() => {
          this.subscriptionSuccess = false;
          this.subscriptionLoading = false;
          window.scrollTo(0, 0);
          this.subscriptionForm.get('email').setValue('');
          this.subscriptionFormView = true;
        }, 3000);
      });
    }


  /**
   * Adventures Initial payload
   * @param {String} adventureId 
   */
  AdventuresInitialPayload(adventureId:string, index: number, adventureGoal: number) {
    this.accountService.byOwner(adventureId).subscribe(
      res => { this.AccountInitialPayload(res, index, adventureGoal, this) }
    );
  }

  /**
   * Account Initial Iteration
   * @param {*} account
   * @param {Component} vm
   */
  AccountInitialIteration (account: any, index: number, adventureGoal: number, vm: this): any {
    if (account.currency == "ARS"){
      this.newest[index].capital = account.amount;
      this.newest[index].percentage = this.newest[index].capital * 100 / adventureGoal;
      this.newest[index].styleExpression = String(this.newest[index].percentage) + '%';
    }
  }
  
  /**
   * Account Initial Payload
   * @param res Response
   */
  AccountInitialPayload(res: any, index: number, adventureGoal: number, vm: this) {
    res.forEach( (account: any) => { vm.AccountInitialIteration(account, index, adventureGoal, vm) } );
    this.loadingImages = false;
  }

    get subscriptionEmailValue() {
      return this.subscriptionForm.get('email');
    }

    get username(): string {
      const credentials = this.authenticationService.credentials;
      return credentials ? credentials.username : null;
    }
  }
