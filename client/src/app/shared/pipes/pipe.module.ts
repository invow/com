import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import { FilterPipe } from './filter.pipe'; // import our pipe here
import { ReversePipe } from './reverse.pipe'; // import our pipe here


@NgModule({
    imports:        [CommonModule],
    declarations:   [ReversePipe, FilterPipe],
    exports:        [ReversePipe, FilterPipe],
})

export class PipeModule { }
