import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class CountriesService {
  baseUrl: string = 'https://restcountries.eu/rest/v2/all';

  constructor(private http: Http) { }

  all() {
    let headers      = new Headers({ fullUrl: true });
    let options = new RequestOptions({
        headers: headers
    });
    return this.http
        .get(this.baseUrl, options)
        .map(res => res.json());
  }
}