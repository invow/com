import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { AuthenticationService } from '../../core/authentication/authentication.service';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AccountService {
  constructor(
    private http: Http,
    private authenticationService: AuthenticationService
  ) {

  }
  account() {
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = new RequestOptions({
            params: { access_token: token }
        }); // Create a request option
        const endpoint = `/checkouts/byUser`;
        return this.http.get(endpoint, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }
  }
  byOwner(owner: string){
    //if(this.authenticationService.isAuthenticated()) {
      //const token = this.authenticationService.credentials.token;
      //let options       = new RequestOptions({
      //    params: { access_token: token }
      //}); // Create a request option
      const endpoint = `/checkouts/byOwner/${owner}`;
      return this.http.get(endpoint)//, options
          .map(res => res.json())
          .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
    //}
  }
}
