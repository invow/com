export class CartItem {
    _id: string;
    adventure_id: string;
    title: string;
    price: number;
    type: string;
    currency: string;
    quantity: number;
    url: string;
}

export class Currency {
    code: string;
    value: number;
}