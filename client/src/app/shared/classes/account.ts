export class Account{
    id: string;
    owner: string;
    currency: string;
    amount: number;
}