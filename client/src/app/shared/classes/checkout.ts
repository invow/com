import { CartItem } from './cart-item'

export class Checkout {
    _id: String;
    firstName: String
    lastName: String;
    email: String;
    phone: String;
    address: String;
    company: String;
    country: String;
    state: String;
    city: String;
    zip: String;
    shipThisAddress: String;
    items: CartItem[];
    createdAt: Date;
    updatedAt: Date;
    payment: any;
    amount: String;
}