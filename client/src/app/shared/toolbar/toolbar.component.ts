import { Component, OnInit, Input, isDevMode } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import  { environment  as environmentProd } from '../../../environments/environment.prod';

import { AuthenticationService } from '../../core/authentication/authentication.service';
import { SharedService } from '../services/shared.service';
import { SearchService } from '../services/main-search.service';
import { Subject } from 'rxjs/Subject';
import { ProfileService } from '../../profile/profile.service';

import { CartService } from '../services/cart.service';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  providers: [SearchService]
})
export class ToolbarComponent implements OnInit {
  items : Array<any> = [];
  currencies : Array<any> = [];
  results: any;
  searchTerm$ = new Subject<string>();
  backgroundColor = '#fff';
  //showSearcher = true;
  env: string;
  user: any;

  //@Input() areVisible: boolean;
  @Input() notifications: number = null;
  @Input() verticals: {
    impulse: boolean;
    job: boolean;
    announcement: boolean;
    skill: boolean;
    guruAgent: boolean;
  };

  search: FormGroup;
  q: string;
  searchButton: boolean;
  showSearchItems: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private fb: FormBuilder,
    public sharedService: SharedService,
    public cartService: CartService,
    private searchService: SearchService,
    private profileService: ProfileService
  ) {
    this.searchButton = true;
    this.showSearchItems = false;
    this.createForm();
    
    this.searchService.search(this.searchTerm$)
    .subscribe(results => {
      this.results = results;
      this.results.forEach((item: any, index: number) => {
        if (!item.image.includes('assets/img/adv-img.png')) {
          this.results[index].image = `${this.env}/image/thumb/${item.image}`;
        }
      });
    });
    if(this.authenticationService.isAuthenticated()) {
      this.profileService.getCurrentUser()
      .subscribe(
        res => { 
          this.user = res;
        },
        err => {
          this.router.navigate([`/error`], { queryParams: { err: JSON.stringify(err)}, replaceUrl: true });
        }
      );
    }
  }

  ngOnInit() {
    /**
     * @todo
     * if sharedService.mode === 'tv'
     * ** @hide search input
     * ** @change background
     * **** @header -> background-color: #030102;
     */
    //console.log(this.router);

    if(isDevMode()) 
      this.env = environment.assetsUrl;
    else
      this.env = environmentProd.assetsUrl;

    //this.areVisible = true;
    this.items = this.getItems();
    this.getSubtotals();
  }

  getItems(){
    if (sessionStorage.getItem("cart"))
      return  JSON.parse(sessionStorage.getItem("cart"))
  }

  getSubtotals(){
    var _t = this;
    var cur : Array<any> = []
    if(this.items && this.items.length !== 0) {
      this.items.forEach(function(item){
        cur[item.currency] = cur[item.currency] ? cur[item.currency] + item.price : item.price;
      })
    }
    this.currencies = []
    Object.keys(cur).forEach(function(c){
      _t.currencies.push({
        code: c,
        value: cur[c]
      })
    })
  }

  remove(i: number){
    this.items.splice(i, 1);
    this.getSubtotals();
  }

  goUser(param: string) {
    this.router.navigate(['/' + param]);
  }

  createForm() {
      this.search = this.fb.group({
          currentSearch: [ this.q, Validators.required ]
      })
  }

  searchBoxChange($event: any) {
    this.q = $event;
  }

  searchNow() {
      let currentSearch = this.search.value.currentSearch;
      if(currentSearch.indexOf('/') !== -1) {
          this.router.navigate(['/search'], { queryParams: { q: currentSearch }});
      } else {
          location.href = `/search?q=${currentSearch}`;
      }
  }

  get username(): string {
    const credentials = this.authenticationService.credentials;
    return credentials ? credentials.username : null;
  }

  get picture(): string {
    const credentials = this.authenticationService.credentials;
    return credentials ? credentials.picture : null;
  }

}
