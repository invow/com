import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { FormsService } from './forms.service'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl,
    AbstractControl
} from '@angular/forms';


@Injectable()
export class UserFormsService {
  pwdPattern = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,12}$";
  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  constructor(
      private fb: FormBuilder,
      private formsService: FormsService
  ) { }

  createLoginForm() {
    return this.fb.group({
        username: [ '',
        Validators.compose([
            Validators.required,
            Validators.email,
            Validators.pattern(this.emailPattern)
        ])],
        password: [ '',
        Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(120)
        ])]
    });
  }

  passwordMatch(c: AbstractControl): { invalid: boolean } {
    if (c.get('password').value !== c.get('passwordRepeated').value) {
        return {invalid: true};
    }
  }

  createSignUpForm() {
    return this.fb.group({
        username: [ '',
        Validators.compose([
            Validators.required,
            Validators.email,
            Validators.pattern(this.emailPattern)
        ])],
        password: [ '',
        Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(120)
        ])],
        passwordRepeated: [ '',
        Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(120),
            Validators.pattern(this.pwdPattern)
        ])]
    }, {
        validator: this.passwordMatch
    });
  }

  createFirstForm() {
    return this.fb.group({
        firstName: [ '',
        Validators.compose([
            Validators.required
        ])],
        lastName: [ '',
        Validators.compose([
            Validators.required
        ])],
        image: [ '',
        Validators.compose([
            Validators.required
        ])],
        shortBio: [ '',
        Validators.compose([
            Validators.required
        ])],
        location: [ '',
        Validators.compose([
            Validators.required
        ])],
        birthday: [ '',
        Validators.compose([
            Validators.required
        ])],
        scope: [ '',
        Validators.compose([
            Validators.required
        ])],
    });
  }

  createAgentForm() {
    return this.fb.group({
        fullName: [ '',
        Validators.compose([
            Validators.required
        ])],
        seniority: [ '',
        Validators.compose([
            Validators.required
        ])],
        identification: [ '',
        Validators.compose([
            Validators.required
        ])],
        email: [ '',
        Validators.compose([
            Validators.required
        ])],
        phone: [ '',
        Validators.compose([
            Validators.required
        ])],
        cv: [ '',
        Validators.compose([
            Validators.required
        ])],
        agreementAccepted: [ '',
        Validators.compose([
            Validators.required
        ])],
    });
  }

}
