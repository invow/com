import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';


@Injectable()
export class FormsService {
  ImpulseTitleMinLengthValidator: number = 3;
  ImpulseTitleMaxLengthValidator: number = 120;

  ImpulsePhoneMinLengthValidator: number = 10;
  ImpulsePhoneMaxLengthValidator: number = 11;

  ImpulseBlurbMinLengthValidator: number = 12;
  ImpulseBlurbMaxLengthValidator: number = 360;

  ImpulseDescriptionMinLengthValidator: number = 120;

  constructor(
      private fb: FormBuilder
  ) { }


  createImpulseForm(impulseObject?: any): FormGroup {
    return this.fb.group({
      step: [
        (impulseObject && impulseObject.step) ? impulseObject.step : 1,
        Validators.required
      ],
      img: [
        (impulseObject && impulseObject.img) ? impulseObject.img : '',
        Validators.required
      ],
      thumbnail: [
        (impulseObject && impulseObject.thumbnail) ? impulseObject.thumbnail : '',
        Validators.required
      ],
      video: [
        (impulseObject && impulseObject.video) ? impulseObject.video : '',
      ],
      ready: [
        (impulseObject && impulseObject.ready) ? impulseObject.ready : false,
        Validators.required
      ],
      adventureType: ['impulse', Validators.required ],
      title: [
        (impulseObject && impulseObject.title) ? impulseObject.title : '',
      Validators.compose([
          Validators.required,
          Validators.minLength(this.ImpulseTitleMinLengthValidator),
          Validators.maxLength(this.ImpulseTitleMaxLengthValidator)
      ])],
      phone: [
        (impulseObject && impulseObject.phone) ? impulseObject.phone : '',
      Validators.compose([
          Validators.required,
          Validators.minLength(this.ImpulsePhoneMinLengthValidator),
          Validators.maxLength(this.ImpulsePhoneMaxLengthValidator)
      ])],
      imageInput: [
        (impulseObject && impulseObject.imageInput) ? impulseObject.imageInput : '',
        Validators.required
      ],
      videoInput: [
        (impulseObject && impulseObject.videoInput) ? impulseObject.videoInput : '',
        //Validators.required
      ],
      videoLink: [
        (impulseObject && impulseObject.videoLink) ? impulseObject.videoLink : '',
        //Validators.required
      ],
      blurb: [
        (impulseObject && impulseObject.blurb) ? impulseObject.blurb : '',
        Validators.compose([
          Validators.required,
          Validators.minLength(this.ImpulseBlurbMinLengthValidator),
          Validators.maxLength(this.ImpulseBlurbMaxLengthValidator)

        ])
      ],
      description: [
        (impulseObject && impulseObject.description) ? impulseObject.description : '',
      Validators.compose([
          Validators.required,
          Validators.minLength(this.ImpulseDescriptionMinLengthValidator)
      ])],
      category: [
        (impulseObject && impulseObject.category) ? impulseObject.category : '',
        Validators.required],
      location: [
        (impulseObject && impulseObject.location) ? impulseObject.location : '',
      Validators.compose([
          Validators.required
      ])],
      /*days: [
      (impulseObject && impulseObject.days) ? impulseObject.days : '',
      Validators.compose([
          //Validators.required,
          Validators.min(1),
          Validators.max(90),
          Validators.pattern(/^\d+$/)
      ])],*/
      goal: [
        (impulseObject && impulseObject.goal) ? impulseObject.goal : '',
      Validators.compose([
          Validators.required,
          Validators.min(1),
          Validators.max(10000000),
          Validators.pattern(/^\d+$/)
      ])],
      collaborators: this.fb.array([ this.createCollaborator((impulseObject && impulseObject.collaborators) ? impulseObject.collaborators : '') ])
    });
  }

  createCollaborator(collaboratorsObject?: any): FormGroup {
      if(collaboratorsObject) {
        return collaboratorsObject;
      } else {
        return this.fb.group({
            email: [
              (collaboratorsObject && collaboratorsObject.email) ? collaboratorsObject.email : '',
              Validators.compose([
                  Validators.required,
                  Validators.email
              ])
            ],
        });
      }
  }

  createCrowdlendingAgreement(crowdlendingObject?: any): FormGroup {
      return this.fb.group({
          index: [
            (crowdlendingObject && crowdlendingObject.index) ? crowdlendingObject.index : 1,
            Validators.required],
          pledgeAmount: [
            (crowdlendingObject && crowdlendingObject.pledgeAmount) ? crowdlendingObject.pledgeAmount : '',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(10000000),
                Validators.pattern(/^\d+$/)
            ])
        ],
        return: [
            (crowdlendingObject && crowdlendingObject.return) ? crowdlendingObject.return : '',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(100),
                Validators.pattern(/^\d+$/)
            ])
        ],
        payments: [
            (crowdlendingObject && crowdlendingObject.payments) ? crowdlendingObject.payments : '',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.pattern(/^\d+$/)
            ])
        ],
        firstPayment: [
        (crowdlendingObject && crowdlendingObject.firstPayment) ? crowdlendingObject.firstPayment : '',
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern(/^\d+$/)
        ])
      ],
      quantity: [
        (crowdlendingObject && crowdlendingObject.quantity) ? crowdlendingObject.quantity : 1,
        Validators.compose([
          Validators.required,
          Validators.min(1),
          Validators.pattern(/^\d+$/)
        ])
      ]
      });
  }

  createCrowdfundingAgreement(crowdfundingObject?: any): FormGroup {
      return this.fb.group({
          index: [
            (crowdfundingObject && crowdfundingObject.index) ? crowdfundingObject.index : 1,
            Validators.required],
          title: [
              (crowdfundingObject && crowdfundingObject.title) ? crowdfundingObject.title : '',
              Validators.compose([
                  Validators.required,
                  Validators.minLength(12),
                  Validators.maxLength(120)
              ])
          ],
          image: [
            (crowdfundingObject && crowdfundingObject.image) ? crowdfundingObject.image : '',
          ],
          pledgeAmount: [
            (crowdfundingObject && crowdfundingObject.pledgeAmount) ? crowdfundingObject.pledgeAmount : '',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(10000000),
                Validators.pattern(/^\d+$/)
            ])
        ],
        shippingTime: [
          (crowdfundingObject && crowdfundingObject.shippingTime) ? crowdfundingObject.shippingTime : '',
          Validators.compose([
            Validators.required,
            Validators.minLength(10),
            Validators.maxLength(10)
          ])
        ],
        createdDate: [
          (crowdfundingObject && crowdfundingObject.createdDate) ? crowdfundingObject.createdDate : '',
          Validators.compose([
          ])
        ],
        days: [
          (crowdfundingObject && crowdfundingObject.days) ? crowdfundingObject.days : '',
          Validators.compose([
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(90)
          ])
        ],
        description: [
            (crowdfundingObject && crowdfundingObject.description) ? crowdfundingObject.description : '',
            Validators.compose([
                Validators.required
            ])
        ],
        quantity: [
          (crowdfundingObject && crowdfundingObject.quantity) ? crowdfundingObject.quantity : 1,
          Validators.compose([
            Validators.required,
            Validators.min(1),
            Validators.pattern(/^\d+$/)
          ])
        ]
      });
  }

  createEquityAgreement(equityObject?: any): FormGroup {
      return this.fb.group({
          index: [
            (equityObject && equityObject.index) ? equityObject.index : 1,
            Validators.required],
          title: [
              (equityObject && equityObject.title) ? equityObject.title : '',
              Validators.compose([])
          ],
          doc: [
            (equityObject && equityObject.doc) ? equityObject.doc : ''
          ],
          fileInput: [
            (equityObject && equityObject.fileInput) ? equityObject.fileInput : ''
          ],
          firstReturn: [
            (equityObject && equityObject.firstReturn) ? equityObject.firstReturn : '',
            Validators.compose([
              Validators.min(1),
              Validators.pattern(/^\d+$/)
            ])
          ],
          price: [
            (equityObject && equityObject.price) ? equityObject.price : '',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(10000000),
                Validators.pattern(/^\d+$/)
            ])
        ],
        participations: [
          (equityObject && equityObject.participations) ? equityObject.participations : '',
          Validators.compose([
              Validators.required,
              Validators.min(1),
              Validators.max(10000000),
              Validators.pattern(/^\d+$/)
          ])
      ],
        percentage: [
            (equityObject && equityObject.percentage) ? equityObject.percentage : '',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(100),
                Validators.pattern(/^\d+$/)
            ])
        ],
        description: [
            (equityObject && equityObject.description) ? equityObject.description : '',
            Validators.compose([
                Validators.required
            ])
        ]
      });
  }

  createChartsForm(chartObject?: any) {
    return this.fb.group({
      title: [
        (chartObject && chartObject.title) ? chartObject.title : '',
        Validators.required
      ],
      description: [
        (chartObject && chartObject.description) ? chartObject.description : '',
        Validators.required
      ],
      charts: (chartObject && chartObject.charts) ? this.fb.array([]) : this.fb.array([ this.createChartForm() ])
    })
  }

  createChartForm(chartObject?: any) {
    return this.fb.group({
      amount: [
        (chartObject && chartObject.amount) ? chartObject.amount : '',
        Validators.compose([
          Validators.required,
          Validators.min(1),
          Validators.pattern(/^\d+$/)
        ])
      ],
      destiny: [
        (chartObject && chartObject.destiny) ? chartObject.destiny : '',
        Validators.required
      ]
    })
  }

  createProposal(): FormGroup {
    return this.fb.group({
      fullName: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(60)
        ])
      ],
      email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email
        ])
      ],
      proposal: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3)
        ])
      ]
    });
  }

  createReview(): FormGroup {
    return this.fb.group({
      title: [
        '',
        Validators.compose([
          Validators.required
        ])
      ],
      email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email
        ])
      ],
      stars: [
        '',
        Validators.compose([
          Validators.required,
          Validators.min(1),
          Validators.max(5),
          Validators.pattern(/^\d+$/)
        ])
      ],
      review: [
        '',
        Validators.compose([
          Validators.required
        ])
      ]
    });
  }

  createDocs(): FormGroup {
    return this.fb.group({
      /*
      type: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(12)
        ])
      ],
      */
      name: [
        '',
        Validators.compose([
          Validators.required
        ])
      ],
      fileInput: [
        '',
        Validators.required
      ],
      /*
      size: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^\d+$/)
        ])
      ],
      */
      public: [
        true,
        Validators.required
      ]
    });
  }

  createJobsForm(jobsObject?: any) {
      return this.fb.group({
          step: ['1', Validators.required ],
          adventureType: ['challenge', Validators.required ],
          title: [
            (jobsObject && jobsObject.title) ? jobsObject.title : '',
          Validators.compose([
              Validators.required,
              Validators.minLength(this.ImpulseTitleMinLengthValidator),
              Validators.maxLength(this.ImpulseTitleMaxLengthValidator)
          ])],
          name: [
            (jobsObject && jobsObject.name) ? jobsObject.name : ''
          ],
          blurb: [
            (jobsObject && jobsObject.blurb) ? jobsObject.blurb : '',
            Validators.compose([
              Validators.required,
              Validators.minLength(12)
            ])
          ],
          description: [
            (jobsObject && jobsObject.description) ? jobsObject.description : '',
            Validators.compose([
               Validators.required,
               Validators.minLength(120)
            ])
          ],
          agreements: this.fb.array([ this.createAgreement()])
      });
  }

  createAgreement(): FormGroup {
      return this.fb.group({
          title: ['',
              Validators.compose([
                  Validators.required,
                  Validators.minLength(12),
                  Validators.maxLength(120)
              ])
          ],
          price: ['',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(10000000),
                Validators.pattern(/^\d+$/)
            ])
          ],
          hours: ['',
              Validators.compose([
                  Validators.required,
                  Validators.min(1),
                  Validators.pattern(/^\d+$/)
              ])
          ],
          description: ['',
              Validators.compose([
                  Validators.required,
                  Validators.minLength(120)
              ])
          ],
          category: ['',
            Validators.required
          ],
          quantity: [1,
            Validators.compose([
              Validators.required,
              Validators.pattern(/^\d+$/)
            ])
          ]
      });
  }

}
