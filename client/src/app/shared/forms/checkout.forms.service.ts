import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { FormsService } from './forms.service'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';


@Injectable()
export class CheckoutFormsService {

  constructor(
      private fb: FormBuilder,
      private formsService: FormsService
  ) { }

  createCheckout(checkoutObject?: any) {
    return this.fb.group({
      firstName: [
        (checkoutObject && checkoutObject.firstName) ? checkoutObject.firstName : '',
        //Validators.required
      ],
      lastName: [
        (checkoutObject && checkoutObject.lastName) ? checkoutObject.lastName : '',
        //Validators.required
      ],
      email: [
        (checkoutObject && checkoutObject.email) ? checkoutObject.email : '',
        //Validators.required
      ],
      phone: [
        (checkoutObject && checkoutObject.phone) ? checkoutObject.phone : null,
        //Validators.required
      ],
      address: [
        (checkoutObject && checkoutObject.address) ? checkoutObject.address : '',
        //Validators.required
      ],
      company: [
        (checkoutObject && checkoutObject.company) ? checkoutObject.company : '',
        //Validators.required
      ],
      country: [
        (checkoutObject && checkoutObject.country) ? checkoutObject.country : '',
        //Validators.required
      ],
      state: [
        (checkoutObject && checkoutObject.state) ? checkoutObject.state : '',
        //Validators.required
      ],
      city: [
        (checkoutObject && checkoutObject.city) ? checkoutObject.city : '',
        //Validators.required
      ],
      zip: [
        (checkoutObject && checkoutObject.zip) ? checkoutObject.zip : '',
        //Validators.required
      ],
      shipThisAddress: [
        (checkoutObject && checkoutObject.shipThisAddress) ? checkoutObject.shipThisAddress : true,
        //Validators.required
      ]
    });

  }
}
