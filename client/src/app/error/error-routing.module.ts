import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { ErrorComponent } from './error.component';

const routes: Routes = Route.withShell([
  { path: 'error', component: ErrorComponent, data: { title: extract('Error') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ErrorRoutingModule { }
