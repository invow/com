import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  name: string;
  status: string;
  message: string;
  error: string = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Response) => {
        const error = params['err'] ? JSON.parse(params['err']) : null;
        
        let BodyString = 
          error &&
          error._body &&
          typeof error._body === 'string';
        const body_ = BodyString ? JSON.parse(error._body) : null;
        const body = !BodyString ? error._body : null;
        
        if(body && body.status !== 0) {
          this.name = body.name;
          this.status = body.status;
          this.message = body.message;
        } else if(body_ && body_.status !== 0) {
          this.name = body_.name;
          this.status = body_.status;
          this.message = body_.message;
        } else this.error = error;

        if(this.message === "token attribute can't be null") {
          this.message = 'Tu token de Mercado Pago expiró.'
          this.name = 'Error de Mercado Pago'
        }
  });

    setTimeout(() => {
      this.router.navigate([`/`], { replaceUrl: true });
    }, 6000)
  }

}
