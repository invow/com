import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdventureService } from '../../shared/adventure.service';
import { CreateService } from '../../create/create.service';
import { FormGroup } from '@angular/forms';
import { FormsService } from '../../shared/forms/forms.service';
import { SharedService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {
  @Input() adventureReviews: any = null;
  @Input() adventureId: string = null;
  @Input() currentSlug: string = null;
  @Input() currentInvcode: string = null;
  @Input() currentUser: boolean = null;
  @Input() refreshReviews: Function = null;

  reviewForm: FormGroup;
  reviews: any;

  reviewFormView: boolean;
  reviewSuccess: boolean;
  reviewError: boolean;

  reviewLoading: boolean;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private adventureService: AdventureService,
      private createService: CreateService,
      private formsService: FormsService,
      public sharedService: SharedService
  ) {
    this.createReviewForm();
  }

  ngOnInit() {
    this.reviews = this.adventureReviews;
    this.reviewSuccess = false;
    this.reviewError = false;
    this.reviewFormView = true;
    
  }

  /**
   * CREATE REVIEW FORM
   */
  createReviewForm() {
    this.reviewForm = this.formsService.createReview();
  }

  /**
   * API MESSAGE
   * @param field 
   */
  apiMessage(field: string) {
    switch(field) {
      case 'reviewOk':
      this.reviewFormView = false;
      this.reviewError = false;
      this.reviewLoading = false;
      this.reviewSuccess = true;
      break;
      case 'reviewError':
      this.reviewFormView = false;
      this.reviewSuccess = false;
      this.reviewLoading = false;
      this.reviewError = true;
      break;
    }
    setTimeout(() => {
      switch(field) {
        case 'reviewOk':
        this.reviewForm.reset();
        this.reviewError = false;
        this.reviewSuccess = false;
        this.reviewLoading = false;
        this.reviewFormView = true;
        break;
        case 'reviewError':
        this.reviewError = false;
        this.reviewSuccess = false;
        this.reviewFormView = true;
        break;
      }
    }, 6000);

  }

  /**
   * SAVE
   * @param type? {String}
   * @param content? {*}
   * @param index? {String}
   */
  save(
    type?: string,
    content?: any,
    index?: string
  ) {
    this.reviewLoading = true;
    this.reviewFormView = false;
    let toSend: any;
    if(!content) {
      this.reviews.push(this.reviewForm.value);
      toSend = {
        reviews: this.reviews
      };
    }
    //after save:
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(
      res => {
        this.reviewLoading = false;
        this.adventureService.getByInvcode(this.currentInvcode)
            .subscribe(
                res => {
                    if(this.currentSlug === res.slug) {
                      this.refreshReviews(this.adventureReviews);
                      this.reviews = this.adventureReviews ? this.adventureReviews : [];
                      this.apiMessage('reviewOk')

                    }
                    else { alert("INCORRECT URL") }
                },
                err => {
                  this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                }
            )

      },
      err => {
        if(err && err.reviews) {
          this.apiMessage('reviewError');
        }
      }
    )
  }
  /**
   * @GETTERS
   * @Forms
   */
  get reviewTitle() { return this.reviewForm.get('title'); }
  get reviewStars() { return this.reviewForm.get('stars'); }
  get reviewEmail() { return this.reviewForm.get('email'); }
  get reviewContent() { return this.reviewForm.get('review'); }
}
