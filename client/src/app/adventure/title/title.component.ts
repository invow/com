import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsService } from '../../shared/forms/forms.service';
import { CreateService } from '../../create/create.service';
import { FormGroup } from '@angular/forms';
import { SharedService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {
  @Input() adventureTitle: string = null;
  @Input() adventureId: string = null;
  @Input() currentUser: string = null;

  adventureForm: FormGroup;
  showTitleInput: boolean;
  showTitleEditButton: boolean;
  

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formsService: FormsService,
      private createService: CreateService,
      public sharedService: SharedService
  ) { }

  ngOnInit() {
    this.showTitleEditButton = false;
    this.showTitleInput = false;
  }

  /**
   * TOGGLE INPUT
   * @param field: string
   */
  toggleInput(field :string) {
    this.showTitleInput = this.showTitleInput ? false : true;
  }

  /**
   * EDIT
   * @param field {String}
   * @param initialContent {*}
   */
  edit(field: string, initialContent: any) {
    this.adventureForm = this.formsService.createImpulseForm(initialContent);
    this.adventureForm.controls.title.setValue(initialContent);
    (<FormGroup>this.adventureForm).removeControl('imageInput');
    this.toggleInput(field);
  }

  /**
   * SAVE
   */
  save() {
    this.sharedService.isLoading = true;
    let toSend: any = {
      title: this.title.value
    };
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(
      (res: any) => {
        this.sharedService.isLoading = false;
        this.showTitleInput = false;
      },
      (err: any) => {
        this.router.navigate([`/error?err=${err}`, err], { replaceUrl: true });
      }
    )
  }

  /**
   * CANCEL
   * @param field String
   */
  cancel(field: string) {
    this.toggleInput(field);
    (<FormGroup>this.adventureForm).removeControl(field);
    this.showTitleEditButton = false;
  }

  /**
   * @GETTER
   * @Forms
   */
  get title() { return this.adventureForm.get('title'); }
}
