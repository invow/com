import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  displayLabel: boolean = false;
  @Input() capital: number = null;
  @Input() goal: number = null;

  constructor(
      private router: Router,
      private route: ActivatedRoute
  ) { }

  ngOnInit() {


  }
}
