import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { FormsService } from '../../shared/forms/forms.service';
import { CreateService } from '../../create/create.service';
import { AdventureService } from '../../shared/adventure.service';
import { SharedService } from '../../shared/services/shared.service';
import { AuthenticationService } from '../../core/authentication/authentication.service';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {
  @Input() adventureDescription: string = null;
  @Input() adventureCharts: any = null;
  @Input() adventureId: string = null;
  @Input() adventureForm: FormGroup;
  @Input() currentInvcode: string = null;
  @Input() currentSlug: string = null;
  @Input() currentUser: boolean = null;

  isLoading: boolean = false;
  descriptionContent: string;

  showDescriptionInput: boolean;
  showDescriptionEditButton: boolean;
  editorConfig: any;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formsService: FormsService,
      private createService: CreateService,
      private adventureService: AdventureService,
      public sharedService: SharedService,
      private authenticationService: AuthenticationService
  ) {
    this.editorConfig = {
      editable: true,
      spellcheck: false,
      height: '24rem',
      minHeight: '5rem',
      placeholder: 'Text here...',
      translate: 'no',
      imageEndPoint: `http://localhost:9000/api/files?access_token=${this.authenticationService.credentials.token}`,
    };
  }

  ngOnInit() {
    this.descriptionContent = this.adventureDescription;
    this.showDescriptionEditButton = false;
    this.showDescriptionInput = false;
  }

  /**
   * EDIT
   * @param field {String}
   * @param initialContent {*}
   */
  edit(field: string, initialContent: any) {
    this.adventureForm = this.formsService.createImpulseForm(initialContent);
    this.adventureForm.controls.description.setValue(initialContent);
    (<FormGroup>this.adventureForm).removeControl('imageInput');
    this.toggleInput(field);
  }

  /**
   * SAVE
   * @param type? {String}
   */
  save(type?: string) {
    this.isLoading = true;
    let toSend: any = {
      description: this.adventureForm.controls.description.value
    };

    this.toggleInput(type);
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(
      res => {
        this.sharedService.isLoading = false;
        this.adventureService.getByInvcode(this.currentInvcode)
        .subscribe(
          res => {
            this.isLoading = false;
            this.showDescriptionInput = false;
            this.descriptionContent = res.description;
          },
          err => {
            this.router.navigate([`/error?err=${err}`, err], { replaceUrl: true });
          }
        )
      },
      err => {
        this.router.navigate([`/error?err=${err}`, err], { replaceUrl: true });
      }
    )
  }

  /**
   * CANCEL
   * @param field String
   */
  cancel(field: string) {
    this.toggleInput(field);
    (<FormGroup>this.adventureForm).removeControl(field);
    this.showDescriptionEditButton = false;
  }

  /**
   * TOGGLE INPUT
   * @param field: string
   */
  toggleInput(field :string) {
    this.showDescriptionInput = this.showDescriptionInput ? false : true;
  }

  /**
   * @GETTER
   * @Forms
   */
  get description() { return this.adventureForm.get('description'); }

}
