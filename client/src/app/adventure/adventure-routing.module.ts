import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { AdventureComponent } from './adventure.component';

const routes: Routes = Route.withShell([
  { path: 'adventure/:invcode/:slug', component: AdventureComponent, data: { title: extract('Adventure') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AdventureRoutingModule { }
