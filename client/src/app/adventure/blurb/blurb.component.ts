import { Component, OnInit, Input } from '@angular/core';
import { CreateService } from '../../create/create.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AdventureService } from '../../shared/adventure.service';
import { SharedService } from '../../shared/services/shared.service';
import { FormGroup } from '@angular/forms';
import { FormsService } from '../../shared/forms/forms.service';

@Component({
  selector: 'app-blurb',
  templateUrl: './blurb.component.html',
  styleUrls: ['./blurb.component.scss']
})
export class BlurbComponent implements OnInit {
  @Input() adventureBlurb: string = null;
  @Input() adventureId: string = null;
  @Input() currentUser: boolean = null;
  @Input() currentSlug: boolean = null;
  @Input() currentInvcode: string = null;
  @Input() adventureForm: FormGroup
  showBlurbInput: boolean;
  showBlurbEditButton: boolean;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private adventureService: AdventureService,
      private createService: CreateService,
      public sharedService: SharedService,
      private formsService: FormsService
  ) { }

  ngOnInit() {
    this.showBlurbEditButton = false;
    this.showBlurbInput = false;
  }

  /**
   * SAVE
   * @param type? {String}
   * @param content? {*}
   * @param index? {String}
   */
  save() {
    this.sharedService.isLoading = true;
    let toSend: any;
    //after save:
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(
      res => {
        this.sharedService.isLoading = false;
        this.adventureService.getByInvcode(this.currentInvcode)
            .subscribe(
                res => {
                    this.showBlurbInput = false;
                },
                err => {
                  this.router.navigate([`/error?err=${err}`, err], { replaceUrl: true });
                }
            )

      },
      err => {
        this.router.navigate([`/error?err=${err}`, err], { replaceUrl: true });
      }
    )
  }

  /**
   * TOGGLE INPUT
   * @param field: string
   */
  toggleInput(field :string) {
    this.showBlurbInput = this.showBlurbInput ? false : true;
  }

  /**
   * CANCEL
   * @param field String
   */
  cancel(field: string) {
    this.toggleInput(field);
    //(<FormGroup>this.adventureForm).removeControl(field);
    this.showBlurbEditButton = false;
  }

  /**
   * EDIT
   * @param field {String}
   * @param initialContent {*}
   */
  edit(field: string, initialContent: any) {
    this.adventureForm.controls.blurb.setValue(initialContent);
    (<FormGroup>this.adventureForm).removeControl('imageInput');
    this.toggleInput(field);
  }

  /**
   * @GETTER
   * @Forms
   */
  get blurb() { return this.adventureForm.get('blurb'); }
}
