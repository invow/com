import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdventureRoutingModule } from './adventure-routing.module';
import { AdventureComponent } from './adventure.component';
import { AdventureService } from '../shared/adventure.service';
import { FormsService } from '../shared/forms/forms.service';
import { SharedModule } from '../shared/shared.module';
import { CreateService } from '../create/create.service';
import { HttpClientModule } from '@angular/common/http';
import { NgxEditorModule } from 'ngx-editor';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { PipeModule } from '../shared/pipes/pipe.module'; // import our pipe here
import { DonationsModule } from './donations/donations.module';
import { DonationsComponent } from './donations/donations.component';
import { RewardsModule } from './rewards/rewards.module';
import { RewardsComponent } from './rewards/rewards.component';
import { BlurbModule } from './blurb/blurb.module';
import { BlurbComponent } from './blurb/blurb.component';
import { HeaderModule } from './header/header.module';
import { HeaderComponent } from './header/header.component';
import { DescriptionModule } from './description/description.module';
import { DescriptionComponent } from './description/description.component';
import { FilesModule } from './files/files.module';
import { FilesComponent } from './files/files.component';
import { MetaModule } from './meta/meta.module';
import { MetaComponent } from './meta/meta.component';
import { ProgressBarModule } from './progress-bar/progress-bar.module';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { ProposalModule } from './proposal/proposal.module';
import { ProposalComponent } from './proposal/proposal.component';
import { ReviewsModule } from './reviews/reviews.module';
import { ReviewsComponent } from './reviews/reviews.component';
import { VideoModule } from './video/video.module';
import { VideoComponent } from './video/video.component';
import { TitleModule } from './title/title.module';
import { TitleComponent } from './title/title.component';
import { ChartsModule } from 'ng2-charts';
import { PieChartModule } from './description/pie-chart/pie-chart.module';
import { PieChartComponent } from './description/pie-chart/pie-chart.component';


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    PopoverModule.forRoot(),
    HttpClientModule,
    NgxEditorModule,
    TranslateModule,
    PipeModule,
    AdventureRoutingModule,
    DonationsModule,
    RewardsModule,
    BlurbModule,
    HeaderModule,
    DescriptionModule,
    FilesModule,
    MetaModule,
    ProgressBarModule,
    ProposalModule,
    TitleModule,
    ReviewsModule,
    VideoModule,
    ChartsModule,
    PieChartModule
  ],
  declarations: [
    AdventureComponent,
    BlurbComponent,
    DescriptionComponent,
    DonationsComponent,
    FilesComponent,
    HeaderComponent,
    MetaComponent,
    PieChartComponent,
    ProgressBarComponent,
    ProposalComponent,
    RewardsComponent,
    ReviewsComponent,
    TitleComponent,
    VideoComponent
  ],
  providers: [
    AdventureService,
    FormsService,
    CreateService,
    AuthenticationService
  ]
})
export class AdventureModule { }
