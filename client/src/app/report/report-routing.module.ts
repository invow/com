import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from '../core/authentication/authentication.guard';
import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { ReportComponent } from './report.component';

const routes: Routes = Route.withShell([
  {
      path: 'report',
      component: ReportComponent,
      canActivate: [AuthenticationGuard],
      data: { title: extract('Report') }
  }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ReportRoutingModule { }
