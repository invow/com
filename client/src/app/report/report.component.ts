import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
    type: string;
    crowdlending: boolean;
    crowdfunding: boolean;
    equity: boolean;
    challenge: boolean;
    campaign: boolean;
    round: boolean;

  constructor(
      private router: Router,
      private route: ActivatedRoute
  ) { }

  ngOnInit() {
      this.route.queryParams.subscribe((params: Response) => {
          console.log(params['type']);
          this.type = params['type'];
          this.type === 'crowdlending' ?
            this.crowdlending = true :
                (this.type === 'crowdfunding') ?
                    this.crowdfunding = true :
                    (this.type === 'equity') ?
                        this.equity = true :
                            (this.type === 'challenge') ?
                                this.challenge = true :
                                    (this.type === 'campaign') ?
                                        this.campaign = true :
                                            (this.type === 'round') ?
                                             this.round = true :
                                                'Go to another place!';

      });
  }

}
