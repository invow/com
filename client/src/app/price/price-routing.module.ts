import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { PriceComponent } from './price.component';

const routes: Routes = Route.withShell([
  { path: 'price', component: PriceComponent, data: { title: extract('Price') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class PriceRoutingModule { }
