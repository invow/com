import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PriceService } from './price.service';
import { CartService } from '../shared/services/cart.service';
import { CartItem } from '../shared/classes/cart-item'
import { AuthenticationService } from '../core/authentication/authentication.service';

import axios from 'axios';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnInit {
  currencyEndpoint: string =
    'http://data.fixer.io/api/latest?access_key=2de38c86a446ae919655aced4f296e4e&format=1';
  prices: any;
  ars: any;

  constructor(
    private router: Router,
    private priceService: PriceService,
    private cartService: CartService,
    private authenticationService: AuthenticationService,
  ) { }

  /**
   * CHECKOUT
   * @param {String} type
   */
  checkout(type: string) {
    let price = type === 'premium' ? 
      this.prices[7].value :
      this.prices[1].value;

    if(this.username) {
      const item =
      [{
        adventure_id: '5ec604c977aa535a841ee22f',
        currency: 'ARS',
        price: price,
        quantity: 1,
        title: `Tarifa ${type} de Invow.`,
        type: type,
        url: `/adventure/14/invow`,
        _id: type
      }];

      this.router.navigate(['/checkout'], { queryParams: { item: JSON.stringify(item) }});
    } else {
      this.router.navigate(['/sign'], { replaceUrl: true });
    }
  }

  ngOnInit() {
    const vm = this;
    this.priceService.getPrices()
      .subscribe(
        async res => {
          if(typeof res !== 'string') {
            this.prices = res;
            axios.get(vm.currencyEndpoint)
            .then((response) => {
              vm.ars = response.data.rates.ARS;
              vm.prices.forEach((item: any, index: number) => {
                vm.prices[index].value = item.value * vm.ars;
              })
            })
            .catch((error) => {
              console.log(error);
            })
          }
        }
      );
  }
  get username(): string {
    const credentials = this.authenticationService.credentials;
    return credentials ? credentials.username : null;
  }
}
