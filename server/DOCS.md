# com v0.0.0



- [About](#about)
	- [Create about](#create-about)
	- [Delete about](#delete-about)
	- [Retrieve about](#retrieve-about)
	- [Retrieve abouts](#retrieve-abouts)
	- [Update about](#update-about)
	
- [Account](#account)
	- [Create account](#create-account)
	- [Delete account](#delete-account)
	- [Retrieve account](#retrieve-account)
	- [Retrieve accounts](#retrieve-accounts)
	- [Update account](#update-account)
	
- [Adventure](#adventure)
	- [Create adventure](#create-adventure)
	- [Delete adventure](#delete-adventure)
	- [Retrieve adventure](#retrieve-adventure)
	- [Retrieve adventures](#retrieve-adventures)
	- [Update adventure](#update-adventure)
	
- [Agent](#agent)
	- [Create agent](#create-agent)
	- [Delete agent](#delete-agent)
	- [Retrieve agent](#retrieve-agent)
	- [Retrieve agents](#retrieve-agents)
	- [Update agent](#update-agent)
	
- [Agreement](#agreement)
	- [Create agreement](#create-agreement)
	- [Delete agreement](#delete-agreement)
	- [Retrieve agreement](#retrieve-agreement)
	- [Retrieve agreements](#retrieve-agreements)
	- [Update agreement](#update-agreement)
	
- [Auth](#auth)
	- [Authenticate](#authenticate)
	- [Authenticate with Facebook](#authenticate-with-facebook)
	- [Authenticate with Github](#authenticate-with-github)
	
- [Bank](#bank)
	- [Create bank](#create-bank)
	- [Delete bank](#delete-bank)
	- [Retrieve bank](#retrieve-bank)
	- [Retrieve banks](#retrieve-banks)
	- [Update bank](#update-bank)
	
- [Category](#category)
	- [Create category](#create-category)
	- [Delete category](#delete-category)
	- [Retrieve categories](#retrieve-categories)
	- [Retrieve category](#retrieve-category)
	- [Update category](#update-category)
	
- [Checkout](#checkout)
	- [Create checkout](#create-checkout)
	- [Delete checkout](#delete-checkout)
	- [Retrieve checkout](#retrieve-checkout)
	- [Retrieve checkouts](#retrieve-checkouts)
	- [Update checkout](#update-checkout)
	
- [Faq](#faq)
	- [Create faq](#create-faq)
	- [Delete faq](#delete-faq)
	- [Retrieve faq](#retrieve-faq)
	- [Retrieve faqs](#retrieve-faqs)
	- [Update faq](#update-faq)
	
- [Favorites](#favorites)
	- [Create favorites](#create-favorites)
	- [Delete favorites](#delete-favorites)
	- [Retrieve favorites](#retrieve-favorites)
	- [Update favorites](#update-favorites)
	
- [File](#file)
	- [Create file](#create-file)
	- [Delete file](#delete-file)
	- [Retrieve file](#retrieve-file)
	- [Retrieve files](#retrieve-files)
	- [Update file](#update-file)
	
- [Fund](#fund)
	- [Create fund](#create-fund)
	- [Delete fund](#delete-fund)
	- [Retrieve fund](#retrieve-fund)
	- [Retrieve funds](#retrieve-funds)
	- [Update fund](#update-fund)
	
- [Investment](#investment)
	- [Create investment](#create-investment)
	- [Delete investment](#delete-investment)
	- [Retrieve investment](#retrieve-investment)
	- [Retrieve investments](#retrieve-investments)
	- [Update investment](#update-investment)
	
- [MainVideo](#mainvideo)
	- [Create main video](#create-main-video)
	- [Delete main video](#delete-main-video)
	- [Retrieve main video](#retrieve-main-video)
	- [Retrieve main videos](#retrieve-main-videos)
	- [Update main video](#update-main-video)
	
- [Movement](#movement)
	- [Create movement](#create-movement)
	- [Delete movement](#delete-movement)
	- [Retrieve movement](#retrieve-movement)
	- [Retrieve movements](#retrieve-movements)
	- [Update movement](#update-movement)
	
- [Operation](#operation)
	- [Create operation](#create-operation)
	- [Retrieve operation](#retrieve-operation)
	- [Retrieve operations](#retrieve-operations)
	- [Update operation](#update-operation)
	
- [Payment](#payment)
	- [Create payment](#create-payment)
	- [Delete payment](#delete-payment)
	- [Retrieve payment](#retrieve-payment)
	- [Retrieve payments](#retrieve-payments)
	- [Update payment](#update-payment)
	
- [Price](#price)
	- [Create price](#create-price)
	- [Retrieve price](#retrieve-price)
	- [Retrieve prices](#retrieve-prices)
	- [Update price](#update-price)
	
- [Product](#product)
	- [Create product](#create-product)
	- [Delete product](#delete-product)
	- [Retrieve product](#retrieve-product)
	- [Retrieve products](#retrieve-products)
	- [Update product](#update-product)
	
- [Profile](#profile)
	- [Create profile](#create-profile)
	- [Delete profile](#delete-profile)
	- [Retrieve profile](#retrieve-profile)
	- [Retrieve profiles](#retrieve-profiles)
	- [Update profile](#update-profile)
	
- [Result](#result)
	- [Create result](#create-result)
	- [Delete result](#delete-result)
	- [Retrieve result](#retrieve-result)
	- [Retrieve results](#retrieve-results)
	- [Update result](#update-result)
	
- [Source](#source)
	- [Create source](#create-source)
	- [Delete source](#delete-source)
	- [Retrieve source](#retrieve-source)
	- [Retrieve sources](#retrieve-sources)
	- [Update source](#update-source)
	
- [SpecialOffer](#specialoffer)
	- [Create special offer](#create-special-offer)
	- [Delete special offer](#delete-special-offer)
	- [Retrieve special offer](#retrieve-special-offer)
	- [Retrieve special offers](#retrieve-special-offers)
	- [Update special offer](#update-special-offer)
	
- [Subscription](#subscription)
	- [Create subscription](#create-subscription)
	- [Delete subscription](#delete-subscription)
	- [Retrieve subscription](#retrieve-subscription)
	- [Retrieve subscriptions](#retrieve-subscriptions)
	- [Update subscription](#update-subscription)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [For email system](#for-email-system)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	
- [Video](#video)
	- [Create video](#create-video)
	- [Delete video](#delete-video)
	- [Retrieve video](#retrieve-video)
	- [Retrieve videos](#retrieve-videos)
	- [Update video](#update-video)
	


# About

## Create about



	POST /abouts


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>About's title.</p>							|
| content			| 			|  <p>About's content.</p>							|
| video			| 			|  <p>About's video.</p>							|

## Delete about



	DELETE /abouts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve about



	GET /abouts/:id


## Retrieve abouts



	GET /abouts


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update about



	PUT /abouts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>About's title.</p>							|
| content			| 			|  <p>About's content.</p>							|
| video			| 			|  <p>About's video.</p>							|

# Account

## Create account



	POST /accounts


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| type			| 			|  <p>Account's type.</p>							|
| owner			| 			|  <p>Account's owner.</p>							|
| amount			| 			|  <p>Account's amount.</p>							|
| currency			| 			|  <p>Account's currency.</p>							|

## Delete account



	DELETE /accounts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve account



	GET /accounts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve accounts



	GET /accounts


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update account



	PUT /accounts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| type			| 			|  <p>Account's type.</p>							|
| owner			| 			|  <p>Account's owner.</p>							|
| amount			| 			|  <p>Account's amount.</p>							|
| currency			| 			|  <p>Account's currency.</p>							|

# Adventure

## Create adventure



	POST /adventures


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>Adventure's title.</p>							|
| capital			| 			|  <p>Adventure's capital.</p>							|
| step			| 			|  <p>Adventure's step.</p>							|
| minimal			| 			|  <p>Adventure's minimal.</p>							|
| img			| 			|  <p>Adventure's img.</p>							|
| video			| 			|  <p>Adventure's video.</p>							|
| description			| 			|  <p>Adventure's description.</p>							|
| category			| 			|  <p>Adventure's category.</p>							|
| location			| 			|  <p>Adventure's location.</p>							|
| days			| 			|  <p>Adventure's days.</p>							|
| goal			| 			|  <p>Adventure's goal.</p>							|
| collaborators			| 			|  <p>Adventure's collaborators.</p>							|
| followers			| 			|  <p>Adventure's followers.</p>							|

## Delete adventure



	DELETE /adventures/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve adventure



	GET /adventures/:id


## Retrieve adventures



	GET /adventures


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update adventure



	PUT /adventures/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>Adventure's title.</p>							|
| capital			| 			|  <p>Adventure's capital.</p>							|
| step			| 			|  <p>Adventure's step.</p>							|
| minimal			| 			|  <p>Adventure's minimal.</p>							|
| img			| 			|  <p>Adventure's img.</p>							|
| video			| 			|  <p>Adventure's video.</p>							|
| description			| 			|  <p>Adventure's description.</p>							|
| category			| 			|  <p>Adventure's category.</p>							|
| location			| 			|  <p>Adventure's location.</p>							|
| days			| 			|  <p>Adventure's days.</p>							|
| goal			| 			|  <p>Adventure's goal.</p>							|
| collaborators			| 			|  <p>Adventure's collaborators.</p>							|
| followers			| 			|  <p>Adventure's followers.</p>							|

# Agent

## Create agent



	POST /agents


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| seniority			| 			|  <p>Agent's seniority.</p>							|
| fullName			| 			|  <p>Agent's fullName.</p>							|
| identification			| 			|  <p>Agent's identification.</p>							|
| email			| 			|  <p>Agent's email.</p>							|
| phone			| 			|  <p>Agent's phone.</p>							|
| cv			| 			|  <p>Agent's cv.</p>							|

## Delete agent



	DELETE /agents/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve agent



	GET /agents/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve agents



	GET /agents


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update agent



	PUT /agents/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| seniority			| 			|  <p>Agent's seniority.</p>							|
| fullName			| 			|  <p>Agent's fullName.</p>							|
| identification			| 			|  <p>Agent's identification.</p>							|
| email			| 			|  <p>Agent's email.</p>							|
| phone			| 			|  <p>Agent's phone.</p>							|
| cv			| 			|  <p>Agent's cv.</p>							|

# Agreement

## Create agreement



	POST /agreements


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>Agreement's title.</p>							|
| amount			| 			|  <p>Agreement's amount.</p>							|
| description			| 			|  <p>Agreement's description.</p>							|
| adventure			| 			|  <p>Agreement's adventure.</p>							|

## Delete agreement



	DELETE /agreements/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve agreement



	GET /agreements/:id


## Retrieve agreements



	GET /agreements


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update agreement



	PUT /agreements/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>Agreement's title.</p>							|
| amount			| 			|  <p>Agreement's amount.</p>							|
| description			| 			|  <p>Agreement's description.</p>							|
| adventure			| 			|  <p>Agreement's adventure.</p>							|

# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

## Authenticate with Facebook



	POST /auth/facebook


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Facebook user accessToken.</p>							|

## Authenticate with Github



	POST /auth/github


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Github user accessToken.</p>							|

# Bank

## Create bank



	POST /banks


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| firstName			| 			|  <p>Bank's firstName.</p>							|
| lastName			| 			|  <p>Bank's lastName.</p>							|
| identification			| 			|  <p>Bank's identification.</p>							|
| accountNumber			| 			|  <p>Bank's accountNumber.</p>							|
| sortCode			| 			|  <p>Bank's sortCode.</p>							|
| user			| 			|  <p>Bank's user.</p>							|

## Delete bank



	DELETE /banks/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve bank



	GET /banks/:id


## Retrieve banks



	GET /banks


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update bank



	PUT /banks/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| firstName			| 			|  <p>Bank's firstName.</p>							|
| lastName			| 			|  <p>Bank's lastName.</p>							|
| identification			| 			|  <p>Bank's identification.</p>							|
| accountNumber			| 			|  <p>Bank's accountNumber.</p>							|
| sortCode			| 			|  <p>Bank's sortCode.</p>							|
| user			| 			|  <p>Bank's user.</p>							|

# Category

## Create category



	POST /categories


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Category's name.</p>							|

## Delete category



	DELETE /categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve categories



	GET /categories


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve category



	GET /categories/:id


## Update category



	PUT /categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Category's name.</p>							|

# Checkout

## Create checkout



	POST /checkouts


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| firstName			| 			|  <p>Checkout's firstName.</p>							|
| lastName			| 			|  <p>Checkout's lastName.</p>							|
| email			| 			|  <p>Checkout's email.</p>							|
| phone			| 			|  <p>Checkout's phone.</p>							|
| address			| 			|  <p>Checkout's address.</p>							|
| company			| 			|  <p>Checkout's company.</p>							|
| country			| 			|  <p>Checkout's country.</p>							|
| state			| 			|  <p>Checkout's state.</p>							|
| city			| 			|  <p>Checkout's city.</p>							|
| zip			| 			|  <p>Checkout's zip.</p>							|
| shipThisAddress			| 			|  <p>Checkout's shipThisAddress.</p>							|
| items			| 			|  <p>Checkout's items.</p>							|

## Delete checkout



	DELETE /checkouts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve checkout



	GET /checkouts/:id


## Retrieve checkouts



	GET /checkouts


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update checkout



	PUT /checkouts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| firstName			| 			|  <p>Checkout's firstName.</p>							|
| lastName			| 			|  <p>Checkout's lastName.</p>							|
| email			| 			|  <p>Checkout's email.</p>							|
| phone			| 			|  <p>Checkout's phone.</p>							|
| address			| 			|  <p>Checkout's address.</p>							|
| company			| 			|  <p>Checkout's company.</p>							|
| country			| 			|  <p>Checkout's country.</p>							|
| state			| 			|  <p>Checkout's state.</p>							|
| city			| 			|  <p>Checkout's city.</p>							|
| zip			| 			|  <p>Checkout's zip.</p>							|
| shipThisAddress			| 			|  <p>Checkout's shipThisAddress.</p>							|
| items			| 			|  <p>Checkout's items.</p>							|

# Faq

## Create faq



	POST /faqs


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>Faq's title.</p>							|
| content			| 			|  <p>Faq's content.</p>							|

## Delete faq



	DELETE /faqs/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve faq



	GET /faqs/:id


## Retrieve faqs



	GET /faqs


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update faq



	PUT /faqs/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>Faq's title.</p>							|
| content			| 			|  <p>Faq's content.</p>							|

# Favorites

## Create favorites



	POST /favorites


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| link			| 			|  <p>Favorites's link.</p>							|
| user			| 			|  <p>Favorites's user.</p>							|

## Delete favorites



	DELETE /favorites/:id


## Retrieve favorites



	GET /favorites


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update favorites



	PUT /favorites/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| link			| 			|  <p>Favorites's link.</p>							|
| user			| 			|  <p>Favorites's user.</p>							|

# File

## Create file



	POST /files


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>File's name.</p>							|
| type			| 			|  <p>File's type.</p>							|
| path			| 			|  <p>File's path.</p>							|

## Delete file



	DELETE /files/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve file



	GET /files/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve files



	GET /files


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update file



	PUT /files/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>File's name.</p>							|
| type			| 			|  <p>File's type.</p>							|
| path			| 			|  <p>File's path.</p>							|

# Fund

## Create fund



	POST /funds


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| concept			| 			|  <p>Fund's concept.</p>							|
| detail			| 			|  <p>Fund's detail.</p>							|
| input			| 			|  <p>Fund's input.</p>							|
| output			| 			|  <p>Fund's output.</p>							|
| amount			| 			|  <p>Fund's amount.</p>							|
| user			| 			|  <p>Fund's user.</p>							|

## Delete fund



	DELETE /funds/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve fund



	GET /funds/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve funds



	GET /funds


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update fund



	PUT /funds/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| concept			| 			|  <p>Fund's concept.</p>							|
| detail			| 			|  <p>Fund's detail.</p>							|
| input			| 			|  <p>Fund's input.</p>							|
| output			| 			|  <p>Fund's output.</p>							|
| amount			| 			|  <p>Fund's amount.</p>							|
| user			| 			|  <p>Fund's user.</p>							|

# Investment

## Create investment



	POST /investments


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| account			| 			|  <p>Investment's account.</p>							|
| adventure			| 			|  <p>Investment's adventure.</p>							|
| name			| 			|  <p>Investment's name.</p>							|
| description			| 			|  <p>Investment's description.</p>							|
| amount			| 			|  <p>Investment's amount.</p>							|
| status			| 			|  <p>Investment's status.</p>							|
| goalAmount			| 			|  <p>Investment's goalAmount.</p>							|
| goalCategory			| 			|  <p>Investment's goalCategory.</p>							|
| goalPlace			| 			|  <p>Investment's goalPlace.</p>							|
| goalMethod			| 			|  <p>Investment's goalMethod.</p>							|

## Delete investment



	DELETE /investments/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve investment



	GET /investments/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve investments



	GET /investments


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update investment



	PUT /investments/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| account			| 			|  <p>Investment's account.</p>							|
| adventure			| 			|  <p>Investment's adventure.</p>							|
| name			| 			|  <p>Investment's name.</p>							|
| description			| 			|  <p>Investment's description.</p>							|
| amount			| 			|  <p>Investment's amount.</p>							|
| status			| 			|  <p>Investment's status.</p>							|
| goalAmount			| 			|  <p>Investment's goalAmount.</p>							|
| goalCategory			| 			|  <p>Investment's goalCategory.</p>							|
| goalPlace			| 			|  <p>Investment's goalPlace.</p>							|
| goalMethod			| 			|  <p>Investment's goalMethod.</p>							|

# MainVideo

## Create main video



	POST /main-videos


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| title			| 			|  <p>Main video's title.</p>							|
| path			| 			|  <p>Main video's path.</p>							|
| user			| 			|  <p>Main video's user.</p>							|
| slug			| 			|  <p>Main video's slug.</p>							|
| time			| 			|  <p>Main video's time.</p>							|
| timeStart			| 			|  <p>Main video's timeStart.</p>							|
| dateStart			| 			|  <p>Main video's dateStart.</p>							|
| link			| 			|  <p>Main video's link.</p>							|
| adventure			| 			|  <p>Main video's adventure.</p>							|
| price			| 			|  <p>Main video's price.</p>							|
| active			| 			|  <p>Main video's active.</p>							|

## Delete main video



	DELETE /main-videos/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve main video



	GET /main-videos/:id


## Retrieve main videos



	GET /main-videos


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update main video



	PUT /main-videos/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| title			| 			|  <p>Main video's title.</p>							|
| path			| 			|  <p>Main video's path.</p>							|
| user			| 			|  <p>Main video's user.</p>							|
| slug			| 			|  <p>Main video's slug.</p>							|
| time			| 			|  <p>Main video's time.</p>							|
| timeStart			| 			|  <p>Main video's timeStart.</p>							|
| dateStart			| 			|  <p>Main video's dateStart.</p>							|
| link			| 			|  <p>Main video's link.</p>							|
| adventure			| 			|  <p>Main video's adventure.</p>							|
| price			| 			|  <p>Main video's price.</p>							|
| active			| 			|  <p>Main video's active.</p>							|

# Movement

## Create movement



	POST /movements


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| from			| 			|  <p>Movement's from.</p>							|
| to			| 			|  <p>Movement's to.</p>							|
| amount			| 			|  <p>Movement's amount.</p>							|
| currency			| 			|  <p>Movement's currency.</p>							|
| concept			| 			|  <p>Movement's concept.</p>							|
| description			| 			|  <p>Movement's description.</p>							|

## Delete movement



	DELETE /movements/:id


## Retrieve movement



	GET /movements/:id


## Retrieve movements



	GET /movements


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update movement



	PUT /movements/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| from			| 			|  <p>Movement's from.</p>							|
| to			| 			|  <p>Movement's to.</p>							|
| amount			| 			|  <p>Movement's amount.</p>							|
| currency			| 			|  <p>Movement's currency.</p>							|
| concept			| 			|  <p>Movement's concept.</p>							|
| description			| 			|  <p>Movement's description.</p>							|

# Operation

## Create operation



	POST /operations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| status			| 			|  <p>Operation's status.</p>							|
| invop			| 			|  <p>Operation's invop.</p>							|
| items			| 			|  <p>Operation's items.</p>							|
| total			| 			|  <p>Operation's total.</p>							|

## Retrieve operation



	GET /operations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve operations



	GET /operations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update operation



	PUT /operations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| status			| 			|  <p>Operation's status.</p>							|
| invop			| 			|  <p>Operation's invop.</p>							|
| items			| 			|  <p>Operation's items.</p>							|
| total			| 			|  <p>Operation's total.</p>							|

# Payment

## Create payment



	POST /payments


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| cardNumber			| 			|  <p>Payment's cardNumber.</p>							|
| expiry			| 			|  <p>Payment's expiry.</p>							|
| code			| 			|  <p>Payment's code.</p>							|
| type			| 			|  <p>Payment's type.</p>							|

## Delete payment



	DELETE /payments/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve payment



	GET /payments/:id


## Retrieve payments



	GET /payments


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update payment



	PUT /payments/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| cardNumber			| 			|  <p>Payment's cardNumber.</p>							|
| expiry			| 			|  <p>Payment's expiry.</p>							|
| code			| 			|  <p>Payment's code.</p>							|
| type			| 			|  <p>Payment's type.</p>							|

# Price

## Create price



	POST /prices


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| type			| 			|  <p>Price's type.</p>							|
| value			| 			|  <p>Price's value.</p>							|
| user			| 			|  <p>Price's user.</p>							|

## Retrieve price



	GET /prices/:id


## Retrieve prices



	GET /prices


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update price



	PUT /prices/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| type			| 			|  <p>Price's type.</p>							|
| value			| 			|  <p>Price's value.</p>							|
| user			| 			|  <p>Price's user.</p>							|

# Product

## Create product



	POST /products


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| item_id			| 			|  <p>Product's item_id.</p>							|
| adventure_id			| 			|  <p>Product's adventure_id.</p>							|
| status			| 			|  <p>Product's status.</p>							|
| report			| 			|  <p>Product's report.</p>							|
| type			| 			|  <p>Product's type.</p>							|

## Delete product



	DELETE /products/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve product



	GET /products/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve products



	GET /products


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update product



	PUT /products/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| item_id			| 			|  <p>Product's item_id.</p>							|
| adventure_id			| 			|  <p>Product's adventure_id.</p>							|
| status			| 			|  <p>Product's status.</p>							|
| report			| 			|  <p>Product's report.</p>							|
| type			| 			|  <p>Product's type.</p>							|

# Profile

## Create profile



	POST /profiles


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| firstName			| 			|  <p>Profile's firstName.</p>							|
| lastName			| 			|  <p>Profile's lastName.</p>							|
| email			| 			|  <p>Profile's email.</p>							|
| birth			| 			|  <p>Profile's birth.</p>							|
| address			| 			|  <p>Profile's address.</p>							|
| town			| 			|  <p>Profile's town.</p>							|
| zip			| 			|  <p>Profile's zip.</p>							|
| city			| 			|  <p>Profile's city.</p>							|
| country			| 			|  <p>Profile's country.</p>							|
| banks			| 			|  <p>Profile's banks.</p>							|
| payments			| 			|  <p>Profile's payments.</p>							|

## Delete profile



	DELETE /profiles/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve profile



	GET /profiles/:id


## Retrieve profiles



	GET /profiles


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update profile



	PUT /profiles/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| firstName			| 			|  <p>Profile's firstName.</p>							|
| lastName			| 			|  <p>Profile's lastName.</p>							|
| email			| 			|  <p>Profile's email.</p>							|
| birth			| 			|  <p>Profile's birth.</p>							|
| address			| 			|  <p>Profile's address.</p>							|
| town			| 			|  <p>Profile's town.</p>							|
| zip			| 			|  <p>Profile's zip.</p>							|
| city			| 			|  <p>Profile's city.</p>							|
| country			| 			|  <p>Profile's country.</p>							|
| banks			| 			|  <p>Profile's banks.</p>							|
| payments			| 			|  <p>Profile's payments.</p>							|

# Result

## Create result



	POST /results


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>Result's name.</p>							|
| latest			| 			|  <p>Result's latest.</p>							|

## Delete result



	DELETE /results/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve result



	GET /results/:id


## Retrieve results



	GET /results


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update result



	PUT /results/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>Result's name.</p>							|
| latest			| 			|  <p>Result's latest.</p>							|

# Source

## Create source



	POST /sources


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| cardNumber			| 			|  <p>Source's cardNumber.</p>							|
| expiry			| 			|  <p>Source's expiry.</p>							|
| code			| 			|  <p>Source's code.</p>							|
| cardType			| 			|  <p>Source's cardType.</p>							|

## Delete source



	DELETE /sources/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve source



	GET /sources/:id


## Retrieve sources



	GET /sources


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update source



	PUT /sources/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| cardNumber			| 			|  <p>Source's cardNumber.</p>							|
| expiry			| 			|  <p>Source's expiry.</p>							|
| code			| 			|  <p>Source's code.</p>							|
| cardType			| 			|  <p>Source's cardType.</p>							|

# SpecialOffer

## Create special offer



	POST /special-offers


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| adventure			| 			|  <p>Special offer's adventure.</p>							|

## Delete special offer



	DELETE /special-offers/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve special offer



	GET /special-offers/:id


## Retrieve special offers



	GET /special-offers


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update special offer



	PUT /special-offers/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| adventure			| 			|  <p>Special offer's adventure.</p>							|

# Subscription

## Create subscription



	POST /subscriptions


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| 			|  <p>Subscription's email.</p>							|

## Delete subscription



	DELETE /subscriptions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve subscription



	GET /subscriptions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve subscriptions



	GET /subscriptions


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update subscription



	PUT /subscriptions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| 			|  <p>Subscription's email.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|
| role			| String			| **optional** <p>User's picture.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## For email system



	GET /users/verify


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|

# Video

## Create video



	POST /videos


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Video's name.</p>							|
| file			| 			|  <p>Video's file.</p>							|
| duration			| 			|  <p>Video's duration.</p>							|
| current			| 			|  <p>Video's current.</p>							|

## Delete video



	DELETE /videos/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve video



	GET /videos/:id


## Retrieve videos



	GET /videos


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update video



	PUT /videos/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Video's name.</p>							|
| file			| 			|  <p>Video's file.</p>							|
| duration			| 			|  <p>Video's duration.</p>							|
| current			| 			|  <p>Video's current.</p>							|


