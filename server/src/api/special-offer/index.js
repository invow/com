import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export SpecialOffer, { schema } from './model'

const router = new Router()
const { adventure } = schema.tree

/**
 * @api {post} /special-offers Create special offer
 * @apiName CreateSpecialOffer
 * @apiGroup SpecialOffer
 * @apiParam adventure Special offer's adventure.
 * @apiSuccess {Object} specialOffer Special offer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Special offer not found.
 */
router.post('/',
  body({ adventure }),
  create)

/**
 * @api {get} /special-offers Retrieve special offers
 * @apiName RetrieveSpecialOffers
 * @apiGroup SpecialOffer
 * @apiUse listParams
 * @apiSuccess {Object[]} specialOffers List of special offers.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /special-offers/:id Retrieve special offer
 * @apiName RetrieveSpecialOffer
 * @apiGroup SpecialOffer
 * @apiSuccess {Object} specialOffer Special offer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Special offer not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /special-offers/:id Update special offer
 * @apiName UpdateSpecialOffer
 * @apiGroup SpecialOffer
 * @apiParam adventure Special offer's adventure.
 * @apiSuccess {Object} specialOffer Special offer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Special offer not found.
 */
router.put('/:id',
  body({ adventure }),
  update)

/**
 * @api {delete} /special-offers/:id Delete special offer
 * @apiName DeleteSpecialOffer
 * @apiGroup SpecialOffer
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Special offer not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
