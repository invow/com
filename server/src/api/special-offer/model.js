import mongoose, { Schema } from 'mongoose'

const specialOfferSchema = new Schema({
  adventure: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

specialOfferSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      adventure: this.adventure,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('SpecialOffer', specialOfferSchema)

export const schema = model.schema
export default model
