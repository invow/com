import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { SpecialOffer } from '.'

const app = () => express(apiRoot, routes)

let specialOffer

beforeEach(async () => {
  specialOffer = await SpecialOffer.create({})
})

test('POST /special-offers 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ adventure: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.adventure).toEqual('test')
})

test('GET /special-offers 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /special-offers/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${specialOffer.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(specialOffer.id)
})

test('GET /special-offers/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /special-offers/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${specialOffer.id}`)
    .send({ adventure: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(specialOffer.id)
  expect(body.adventure).toEqual('test')
})

test('PUT /special-offers/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ adventure: 'test' })
  expect(status).toBe(404)
})

test('DELETE /special-offers/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${specialOffer.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /special-offers/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${specialOffer.id}`)
  expect(status).toBe(401)
})

test('DELETE /special-offers/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
