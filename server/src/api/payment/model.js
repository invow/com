import mongoose, { Schema } from 'mongoose'

const paymentSchema = new Schema({
    fullName: {
        type: String,
        minlength: 3,
        maxlength: 30
    },
    birthday: {
        type: String
    },
    email: {
        type: String
    },
    phone: {
        type: Number,
        minlength: 3,
        maxlength: 30
    },
    address: {
        type: String,
        minlength: 6,
        maxlength: 30
    },
    city: {
        type: String,
        minlength: 6,
        maxlength: 60
    },
    country: {
        type: String,
        minlength: 6,
        maxlength: 60
    },
    zip: {
        type: Number,
        min: 3,
        max: 30
    },
    identification: {
        type: Number,
        minlength: 3,
        maxlength: 30
    },
    idFront: {
      type: String
    },
    idBack: {
      type: String
    },
    accountSelected: String,
    paymentSourceSelected: String,
    bankAccounts: [{
      ownerName: {
        type: String,
        minlength: 3,
        maxlength: 60
      },
      ownerId: {
        type: Number,
        minlength: 3,
        maxlength: 30
      },
      accountNumber: {
        type: Number,
        minlength: 16,
        maxlength: 16
      }
    }],
    paymentSources: [{
      accountEmail: String,
      accountType: {
        type: String,
        minlength: 4,
        maxlength: 10
      }
    }],
    adventure: {
      type: Schema.ObjectId,
      ref: 'Adventure',
      required: true
    },
    user: {
      type: Schema.ObjectId,
      ref: 'User',
      required: true
    }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

paymentSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      fullName: this.fullName,
      birthday: this.birthday,
      email: this.email,
      phone: this.phone,
      address: this.address,
      city: this.city,
      country: this.country,
      zip: this.zip,
      identification: this.identification,
      adventure: this.adventure.view(full),
      user: this.user.view(full),
      accountSelected: this.accountSelected,
      paymentSourceSelected: this.paymentSourceSelected,
      bankAccounts: this.bankAccounts,
      paymentSources: this.paymentSources,
      idFront: this.idFront,
      idBack: this.idBack,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Payment', paymentSchema)

export const schema = model.schema
export default model
