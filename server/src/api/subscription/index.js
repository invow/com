import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Subscription, { schema } from './model'

const router = new Router()
const { email } = schema.tree

/**
 * @api {post} /subscriptions Create subscription
 * @apiName CreateSubscription
 * @apiGroup Subscription
 * @apiParam email Subscription's email.
 * @apiSuccess {Object} subscription Subscription's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Subscription not found.
 */
router.post('/',
  body({ email }),
  create)

/**
 * @api {get} /subscriptions Retrieve subscriptions
 * @apiName RetrieveSubscriptions
 * @apiGroup Subscription
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} subscriptions List of subscriptions.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  master(),
  query(),
  index)

/**
 * @api {get} /subscriptions/:id Retrieve subscription
 * @apiName RetrieveSubscription
 * @apiGroup Subscription
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} subscription Subscription's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Subscription not found.
 * @apiError 401 master access only.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /subscriptions/:id Update subscription
 * @apiName UpdateSubscription
 * @apiGroup Subscription
 * @apiParam email Subscription's email.
 * @apiSuccess {Object} subscription Subscription's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Subscription not found.
 */
router.put('/:id',
  body({ email }),
  update)

/**
 * @api {delete} /subscriptions/:id Delete subscription
 * @apiName DeleteSubscription
 * @apiGroup Subscription
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Subscription not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
