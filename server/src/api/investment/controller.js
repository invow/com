import { success, notFound, authorOrAdmin } from '../../services/response/'
import { Investment } from '.'

export const create = ({ user, bodymen: { body } }, res, next) =>
  Investment.create({ ...body, user })
    .then((investment) => investment.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Investment.find(query, select, cursor)
    .populate('user')
    .then((investments) => investments.map((investment) => investment.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Investment.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((investment) => investment ? investment.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  Investment.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((investment) => investment ? Object.assign(investment, body).save() : null)
    .then((investment) => investment ? investment.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Investment.findById(params.id)
    .then(notFound(res))
    .then((investment) => investment ? investment.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
