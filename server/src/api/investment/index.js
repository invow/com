import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token, master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Investment, { schema } from './model'

const router = new Router()
const { account, adventure, name, description, amount, status, goalAmount, goalCategory, goalPlace, goalMethod } = schema.tree

/**
 * @api {post} /investments Create investment
 * @apiName CreateInvestment
 * @apiGroup Investment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam account Investment's account.
 * @apiParam adventure Investment's adventure.
 * @apiParam name Investment's name.
 * @apiParam description Investment's description.
 * @apiParam amount Investment's amount.
 * @apiParam status Investment's status.
 * @apiParam goalAmount Investment's goalAmount.
 * @apiParam goalCategory Investment's goalCategory.
 * @apiParam goalPlace Investment's goalPlace.
 * @apiParam goalMethod Investment's goalMethod.
 * @apiSuccess {Object} investment Investment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Investment not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ account, adventure, name, description, amount, status, goalAmount, goalCategory, goalPlace, goalMethod }),
  create)

/**
 * @api {get} /investments Retrieve investments
 * @apiName RetrieveInvestments
 * @apiGroup Investment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} investments List of investments.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /investments/:id Retrieve investment
 * @apiName RetrieveInvestment
 * @apiGroup Investment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} investment Investment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Investment not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /investments/:id Update investment
 * @apiName UpdateInvestment
 * @apiGroup Investment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam account Investment's account.
 * @apiParam adventure Investment's adventure.
 * @apiParam name Investment's name.
 * @apiParam description Investment's description.
 * @apiParam amount Investment's amount.
 * @apiParam status Investment's status.
 * @apiParam goalAmount Investment's goalAmount.
 * @apiParam goalCategory Investment's goalCategory.
 * @apiParam goalPlace Investment's goalPlace.
 * @apiParam goalMethod Investment's goalMethod.
 * @apiSuccess {Object} investment Investment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Investment not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ account, adventure, name, description, amount, status, goalAmount, goalCategory, goalPlace, goalMethod }),
  update)

/**
 * @api {delete} /investments/:id Delete investment
 * @apiName DeleteInvestment
 * @apiGroup Investment
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Investment not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
