import mongoose, { Schema } from 'mongoose'

const favoritesSchema = new Schema({
  link: {
    type: String
  },
  user: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

favoritesSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      link: this.link,
      user: this.user,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Favorites', favoritesSchema)

export const schema = model.schema
export default model
