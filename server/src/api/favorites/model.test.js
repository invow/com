import { Favorites } from '.'

let favorites

beforeEach(async () => {
  favorites = await Favorites.create({ link: 'test', user: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = favorites.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(favorites.id)
    expect(view.link).toBe(favorites.link)
    expect(view.user).toBe(favorites.user)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = favorites.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(favorites.id)
    expect(view.link).toBe(favorites.link)
    expect(view.user).toBe(favorites.user)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
