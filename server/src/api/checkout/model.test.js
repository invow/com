import { Checkout } from '.'

let checkout

beforeEach(async () => {
  checkout = await Checkout.create({ firstName: 'test', lastName: 'test', email: 'test', phone: 'test', address: 'test', company: 'test', country: 'test', state: 'test', city: 'test', zip: 'test', shipThisAddress: 'test', items: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = checkout.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(checkout.id)
    expect(view.firstName).toBe(checkout.firstName)
    expect(view.lastName).toBe(checkout.lastName)
    expect(view.email).toBe(checkout.email)
    expect(view.phone).toBe(checkout.phone)
    expect(view.address).toBe(checkout.address)
    expect(view.company).toBe(checkout.company)
    expect(view.country).toBe(checkout.country)
    expect(view.state).toBe(checkout.state)
    expect(view.city).toBe(checkout.city)
    expect(view.zip).toBe(checkout.zip)
    expect(view.shipThisAddress).toBe(checkout.shipThisAddress)
    expect(view.items).toBe(checkout.items)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = checkout.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(checkout.id)
    expect(view.firstName).toBe(checkout.firstName)
    expect(view.lastName).toBe(checkout.lastName)
    expect(view.email).toBe(checkout.email)
    expect(view.phone).toBe(checkout.phone)
    expect(view.address).toBe(checkout.address)
    expect(view.company).toBe(checkout.company)
    expect(view.country).toBe(checkout.country)
    expect(view.state).toBe(checkout.state)
    expect(view.city).toBe(checkout.city)
    expect(view.zip).toBe(checkout.zip)
    expect(view.shipThisAddress).toBe(checkout.shipThisAddress)
    expect(view.items).toBe(checkout.items)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
