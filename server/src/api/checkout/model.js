import mongoose, { Schema } from 'mongoose'

const checkoutSchema = new Schema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: String
  },
  address: {
    type: String
  },
  company: {
    type: String
  },
  country: {
    type: String
  },
  state: {
    type: String
  },
  city: {
    type: String
  },
  zip: {
    type: Number
  },
  shipThisAddress: {
    type: Boolean
  },
  items: [{
      adventure_id: {
        type: String
      },
      currency: {
        type: String
      },
      price: {
        type: Number
      },
      quantity: {
        type: Number
      },
      title: {
        type: String
      },
      type: {
        type: String
      },
      url: {
        type: String
      },
      _id: {
        type: String
      }
  }]
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

checkoutSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      phone: this.phone,
      address: this.address,
      company: this.company,
      country: this.country,
      state: this.state,
      city: this.city,
      zip: this.zip,
      shipThisAddress: this.shipThisAddress,
      items: this.items,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Checkout', checkoutSchema)

export const schema = model.schema
export default model
