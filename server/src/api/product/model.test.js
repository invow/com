import { Product } from '.'
import { User } from '../user'

let user, product

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  product = await Product.create({ user, item_id: 'test', adventure_id: 'test', status: 'test', report: 'test', type: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = product.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(product.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.item_id).toBe(product.item_id)
    expect(view.adventure_id).toBe(product.adventure_id)
    expect(view.status).toBe(product.status)
    expect(view.report).toBe(product.report)
    expect(view.type).toBe(product.type)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = product.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(product.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.item_id).toBe(product.item_id)
    expect(view.adventure_id).toBe(product.adventure_id)
    expect(view.status).toBe(product.status)
    expect(view.report).toBe(product.report)
    expect(view.type).toBe(product.type)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
