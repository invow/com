import mongoose, { Schema } from 'mongoose'
var autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(mongoose)
import mongooseKeywords from 'mongoose-keywords'
//const mongoosePatchUpdate = require('mongoose-patch-update')

const adventureSchema = new Schema({
  invcode: {
    type: Number,
    min: 1
  },
  title: {
    type: String,
    minlength: 3,
    maxlength: 120,
  },
  capital: {
    type: Number
  },
  backers: {
    type: Number
  },
  blurb: {
    type: String,
    minlength: 12,
    maxlength: 360
  },
  minimal: {
    type: Number
  },
  img: {
    type: String
  },
  thumbnail: {
      type: String
  },
  video: {
    type: String
  },
  videoLink: {
    type: String
  },
  embed: {
    type: String
  },
  description: {
    type: String,
    minlength: 120
  },
  category: {
    type: String
  },
  location: {
    type: String
  },
  identification: {
      type: Number,
      minlength: 3,
      maxlength: 99
  },
  days: {
    type: Number,
    min: 1,
    max: 90
  },
  goal: {
    type: Number,
    min: 1,
    max:10000000
  },
  percentage: {
    type: Number,
    min: 1,
    max: 100
  },
  collaborators: [{
		email: {
      type: String
    }
	}],
  followers: [{
    user: {
      type: String
    }
  }],
  likes: [{
    user: {
      type: String
    }
  }],
  step: {
    type: Number,
    min: 1
  },
  adventureType: {
    type: String
  },
  slug: {
      type: String
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  priv: {
    type: Boolean
  },
  funded: {
    type: Boolean
  },
  ready: {
    type: Boolean
  },
  credits: [{
      index: {
        type: Number,
        min: 1
      },
      pledgeAmount: {
        type: Number,
        min: 1,
        max: 10000000
      },
      return: {
        type: Number,
        min: 1,
        max: 100
      },
      payments: {
        type: Number,
        min: 1
      },
      firstPayment: {
        type: Number,
        min: 1
      },
      quantity: {
        type: Number,
        min: 1
      }
  }],
  rewards: [{
    createdDate: {type: Date},
    days: {type: Number},
    index: {
      type: Number,
      min: 1
    },
    title: {
      type: String,
      minlength: 12,
      maxlength: 120
    },
    image: {
      type: String
    },
    pledgeAmount: {
      type: Number,
      min: 1,
      max: 10000000
    },
    shippingTime: {
      type: Number,
      min: 1
    },
    description: {
      type: String
    },
    quantity: {
      type: Number,
      min: 0
    }
  }],
  participations: [{
    index:{
      type: Number,
      min: 1
    },
    title: {
      type: String
    },
    doc: {
      type: String
    },
    fileInput: {
      type: String
    },
    firstReturn: {
      type: Number,
      min: 1
    },
    price: {
      type: Number,
      min: 1,
      max: 10000000
    },
    participations: {
      type: Number,
      min: 1,
      max: 10000000
    },
    percentage: {
      type: Number,
      min: 1,
      max: 100
    },
    description: {
      type: String
    }
  }],
  charts: [{
    title: String,
    description: String,
    charts: [{
      amount: Number,
      destiny: String
    }]
  }],
  proposals: [{
    fullName: {
      type: String,
      minlength: 3,
      maxlength: 60
    },
    email: {
      type: String
    },
    proposal: {
      type: String,
      minlength: 3
    },
    user: {
      type: Schema.ObjectId,
      ref: 'User'
    }
  }],
  contributions: [{
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    contributor: {
      type: String
    },
    email: {
      type: String
    },
    invop: {
      type: String
    },
    amount: {
      type: Number
    },
    status: {
      type: String
    }
  }],
  phone: {
    type: Number
  },
  delivery: [{
    checkoutDate: {
      type: Date
    },
    reward: {
      id: {
        type: String
      },
      title: {
        type: String
      },
    },
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    contributor: {
      type: String
    },
    address: {
      type: String
    },
    phone: {
      type: Number
    },
    email: {
      type: String
    },
    invop: {
      type: String
    },
    amount: {
      type: Number
    },
    status: {
      type: String
    },
    shipCompany: {
      type: String
    },
    shipTrackingCode: {
      type: String
    }
  }],
  documents: [{
    type: {
      type: String,
      minlength: 2,
      maxlength: 12
    },
    name: String,
    size: Number,
    user: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    public: Boolean
  }],
  reviews: [{
    title: String,
    email: String,
    stars: {
      type: Number,
      min: 1,
      max: 5
    },
    user: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    review: String
  }],
  // JOBS //
  name: {
    type: String
  },
  agreements: [{
    title: {
      type: String
    },
    price: {
      type: Number,
      min: 1,
      max: 10000000
    },
    hours: {
      type: Number,
      min: 1
    },
    description: {
      type: String,
      minlength: 120
    },
    category: {
      type: String
    }
  }]
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

adventureSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      invcode: this.invcode,
      title: this.title,
      name: this.name,
      agreements: this.agreements,
      capital: this.capital,
      backers: this.backers,
      reviews: this.reviews,
      documents: this.documents,
      proposals: this.proposals,
      contributions: this.contributions,
      delivery: this.delivery,
      charts: this.charts,
      blurb: this.blurb,
      minimal: this.minimal,
      img: this.img,
      thumbnail: this.thumbnail,
      video: this.video,
      videoLink: this.videoLink,
      embed: this.embed,
      description: this.description,
      category: this.category,
      location: this.location,
      identification: this.identification,
      days: this.days,
      goal: this.goal,
      percentage: this.percentage,
      collaborators: this.collaborators,
      followers: this.followers,
      likes: this.likes,
      step: this.step,
      adventureType: this.adventureType,
      slug: this.slug,
      user: this.user.view(full),
      priv: this.priv,
      funded: this.funded,
      ready: this.ready,
      credits: this.credits,
      rewards: this.rewards,
      participations: this.participations,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}


adventureSchema.plugin(autoIncrement.plugin, {
  model: 'Adventure',
  field: 'invcode',
  startAt: 1
})
//adventureSchema.plugin(mongooseKeywords, { paths: ['invcode'] })
const model = mongoose.model('Adventure', adventureSchema)
//adventureSchema.plugin(mongoosePatchUpdate)

export const schema = model.schema
export default model
