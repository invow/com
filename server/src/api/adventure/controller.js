import { success, notFound } from '../../services/response/'
import { Adventure } from '.'
import { Account } from '../account/'
import { Result } from '../result'
import { Profile } from '../profile'

import formidable from 'formidable'
//import sharp from 'sharp'
import fs from 'fs'
import path from 'path'
const uploadDir = path.join(__dirname, '/..', '/..', '/..', '/uploads/') //i made this  before the function because i use it multiple times for deleting later
const imageDir = uploadDir + 'images/'
const videoDir = uploadDir + 'videos/'
const docDir = uploadDir + 'docs/'

export const create = ({ bodymen: { body }, user }, res, next) =>
{
    function string_to_slug (str) {
        str = str.replace(/^\s+|\s+$/g, '') // trim
        str = str.toLowerCase()

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
        var to   = "aaaaeeeeiiiioooouuuunc------"
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-') // collapse dashes

        return str
    }

    body.user = user
    body.slug = string_to_slug(body.title)
    body.contributions = []
    body.delivery = []

    if(body.adventureType === 'impulse') {
      body.backers = 0
      body.capital = 0
      body.days = body.days ? body.days : 60;

      if (body.videoLink.includes('https://vimeo')) {
        body.embed = `https://player.vimeo.com/video/${body.videoLink.substr(18)}`
      } else {
        const idGetter = require('youtube-link-to-id')
        const ids = idGetter.linkStringToIds(body.videoLink)
        body.embed = `https://www.youtube.com/embed/${ids[0]}`
      }
    }

/*
    //Creating a thumbnail
    sharp(`./uploads/${body.img}`)
      .resize(320, 240)
      // Side add: 650, 455
      //.resize(373, 425)
      //.crop(sharp.strategy.entropy)
      //.crop(sharp.strategy.attention)
      .toFile(`./uploads/${body.thumbnail}`, (err, info) => {
          if(err) {
              console.log(' --- :Error: --- ', err)
          }
          if(info) {
              console.log(' --- :Info: Image resized --- ', info)
          }
      })
*/
  //////
  var currencies = ["ARS", "USD"];

  Adventure.create(body)
    .then((adventure) => {

      let result = {};
      result = new Result()
      result.invcode = adventure.invcode
      result.name = adventure.title
      result.slug = adventure.slug
      result.image = `${adventure.img}`
      result.blurb = adventure.blurb
      result.days = adventure.days
      result.save()


      Profile.findOne({ username: user.name })
      .then(notFound(res))
      .then((profile) => {
        profile.adventures.push(adventure.invcode)
        profile.save()
      })
      _recursiveAccountCreation(0, currencies, adventure._id);

      return adventure.view(true)

    })
    .then(success(res, 201))
    .catch(next)
}

function _recursiveAccountCreation(index, currencies, owner, callback){
  if (currencies.length){
    var newAccount = new Account();
    newAccount.owner = owner;
    newAccount.currency = currencies[index];
    newAccount.amount = 0;
    newAccount.save(function(err, data){
      if (err){
        throw err
      }
      else{
        if (data){
          index++;
          if (index < currencies.length)
            _recursiveAccountCreation(index, currencies, owner, callback);
          else
            callback && callback(data);
        }
      }
    })
  }
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Adventure.find(query, select, cursor)
    .then((adventures) => adventures.map((adventure, index) => {

      console.log('ADVENTURE ::: ', adventure)

      const filesUrl = __dirname.replace('src/api/adventure', 'uploads/images')
      try {
        if (fs.existsSync(`${filesUrl}/thumb/${adventure.img}`)) {
          adventure.img = `${adventure.img}`;
        } else {
          adventure.img = `assets/img/adv-img.png`;
        }
      } catch(err) {
        throw err
      }
      return adventure.view()
    }))
    .then(success(res))
    .catch(next)

export const byInvcode = ({ params }, res, next) =>
  Adventure.findOne({ invcode: params.invcode })
    .then(notFound(res))
    .then((adventure) => adventure ? adventure.view() : null)
    .then(success(res))
    .catch(next)

export const byType = ({ params }, res, next) =>
  Adventure.findOne({ adventureType: params.adventureType })
    .then(notFound(res))
    .then((adventure) => adventure ? adventure.view() : null)
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Adventure.findById(params.id)
    .then(notFound(res))
    .then((adventure) => adventure ? adventure.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
{
  const newDelivery = body.delivery ? body.delivery : null
  //chequear si el delivery es distinto >>> profile.findone > actualizar las rewards
  function clean(obj) {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj;
  }

  Adventure.findById(params.id)
    .then(notFound(res))
    .then((adventure) => {
      if(newDelivery && JSON.stringify(adventure.delivery) !== JSON.stringify(newDelivery)) {
        adventure.delivery.forEach(item => {
          newDelivery.forEach(newItem => {
            if(item.invop === newItem.invop) {
                Profile.findOne({ username: item.contributor })
                .then(notFound(res))
                .then((profile) => {
                  profile.rewards.forEach((reward, index) => {
                    if(reward.invop === item.invop) {
                      profile.rewards[index].shipCompany = newItem.shipCompany
                      profile.rewards[index].shipTrackingCode = newItem.shipTrackingCode
                      profile.rewards[index].status = newItem.status
                    }
                  })
                  profile.save()
                  return profile
                })
            }
          })
        })
      }
      return adventure ? Object.assign(adventure, clean(body)).save() : null
    })
    .then(adventure => adventure ? adventure.view(true) : null)
    .then(success(res))
    .catch(next)
}

export const destroy = ({ params }, res, next) =>
  Adventure.findById(params.id)
    .then(notFound(res))
    .then((adventure) => adventure ? adventure.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)

export const upload = (req, res, next) =>
{
    var form = new formidable.IncomingForm()
    form.multiples = true
    form.keepExtensions = true
    form.uploadDir = uploadDir
    form.maxFileSize = 5000000000
    form.parse(req, (err, fields, files) => {

      if (err) {
        console.log('------------- /upload ERROR -------------')
        console.log(err)
        console.log('------------- END /upload ERROR -------------')
        return res.status(500).json({ error: err })
      }

      if(files.image) {
          if(files.image.type === 'image/jpeg') {
              if(files.image.size <= 50000000) {
                  console.log(' ------------- LOG ------------ ')
                  console.log(files.image.size)
                  console.log(' ------------- /LOG ------------ ')
                  uploadDir
              } else {
                  console.log('Wrong size!')
              }
          } else {
              console.log('Wrong type!');
          }
      } else if(files.video) {
           if(files.video.type === 'video/mp4') {
               if(files.video.size <= 5000000000) {
                   console.log(' ------------- LOG ------------ ')
                   console.log(files.video.size)
                   console.log(' ------------- /LOG ------------ ')
               } else {
                   console.log('Wrong size!')
               }
           } else {
               console.log('Wrong type!');
           }

      } else if(files.doc) {
        if(
          files.doc.type === 'application/pdf' ||
          files.doc.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          files.doc.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          files.doc.type === 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        ) {
          console.log('------------- IS A VALID DOC ------------')
        }
      }
    })
    form.on('fileBegin', function (name, file) {
      const [fileName, fileExt] = file.name.split('.')
      let completeName = `${fileName}_${new Date().getTime()}.${fileExt}`
      let extLower = fileExt.toLowerCase()
      let validDoc = (
        extLower === 'pdf' ||
        extLower === 'docx' ||
        extLower === 'xlsx' ||
        extLower === 'pptx'
      )

      let validImage = (
        extLower === 'jpg' ||
        extLower === 'jpeg' ||
        extLower === 'png' ||
        extLower === 'gif'
      )

      let validVideo = (
        extLower === 'mp4' ||
        extLower === 'mkv' ||
        extLower === 'm4v' ||
        extLower === 'webm' ||
        extLower === 'ogv'
      )

      if(validDoc) { file.path = path.join(docDir, completeName) }
      else  if(validImage) { file.path = path.join(imageDir, completeName) }
      else if(validVideo) { file.path = path.join(videoDir, completeName) }

      if(JSON.stringify(file).size !== 0) {
        if(validDoc || validImage || validVideo) {
                //////
                //var thumb = require('node-thumbnail').thumb
                var im = require('imagemagick')

                fs.watch(`./uploads/images/`, function(data){
                  if(data === 'rename') {
                    if (fs.existsSync(`./uploads/images/${completeName}`)) {
                        // im.crop({
                        //   srcPath: `./uploads/images/${completeName}`,
                        //   dstPath: `./uploads/images/thumb/${completeName}`,
                        //   width: 400,
                        //   height: 400,
                        //   quality: 1,
                        //   gravity: "Center"
                        // }, function(err, stdout, stderr){
                        //   if(err) {
                        //     console.log(err);
                        //   }
                        //   if(stdout) {
                        //     console.log(stdout);
                        //   }
                        //   if(stderr) {
                        //     console.log(stderr);
                        //   }
                        //   console.log('resized ');
                        // });
                    }
                  }
                })

                // thumb(options, callback);
          /*
                thumb({
                  source: `./uploads/images/thumb/${body.img}`, // could be a filename: dest/path/image.jpg
                  destination: `./uploads/images/thumb/`,
                  concurrency: 4
                }, function(files, err, stdout, stderr) {
                  console.log(`Thumb for ${body.img} done!`)
                })
                */
        }
        res.status(200).json({
            uploaded: true,
            name: completeName,
            thumbnail: `${fileName}_${new Date().getTime()}_thumb.${fileExt}`
        })
      }
    })
}
