import { success, notFound } from '../../services/response/'
import { Movement } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Movement.create(body)
    .then((movement) => movement.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Movement.find(query, select, cursor)
    .then((movements) => movements.map((movement) => movement.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Movement.findById(params.id)
    .then(notFound(res))
    .then((movement) => movement ? movement.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Movement.findById(params.id)
    .then(notFound(res))
    .then((movement) => movement ? Object.assign(movement, body).save() : null)
    .then((movement) => movement ? movement.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Movement.findById(params.id)
    .then(notFound(res))
    .then((movement) => movement ? movement.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
