import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Movement } from '.'

const app = () => express(apiRoot, routes)

let movement

beforeEach(async () => {
  movement = await Movement.create({})
})

test('POST /movements 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ from: 'test', to: 'test', amount: 'test', currency: 'test', concept: 'test', description: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.from).toEqual('test')
  expect(body.to).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.currency).toEqual('test')
  expect(body.concept).toEqual('test')
  expect(body.description).toEqual('test')
})

test('GET /movements 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /movements/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${movement.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(movement.id)
})

test('GET /movements/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /movements/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${movement.id}`)
    .send({ from: 'test', to: 'test', amount: 'test', currency: 'test', concept: 'test', description: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(movement.id)
  expect(body.from).toEqual('test')
  expect(body.to).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.currency).toEqual('test')
  expect(body.concept).toEqual('test')
  expect(body.description).toEqual('test')
})

test('PUT /movements/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ from: 'test', to: 'test', amount: 'test', currency: 'test', concept: 'test', description: 'test' })
  expect(status).toBe(404)
})

test('DELETE /movements/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${movement.id}`)
  expect(status).toBe(204)
})

test('DELETE /movements/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
