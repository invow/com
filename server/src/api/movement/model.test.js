import { Movement } from '.'

let movement

beforeEach(async () => {
  movement = await Movement.create({ from: 'test', to: 'test', amount: 'test', currency: 'test', concept: 'test', description: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = movement.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(movement.id)
    expect(view.from).toBe(movement.from)
    expect(view.to).toBe(movement.to)
    expect(view.amount).toBe(movement.amount)
    expect(view.currency).toBe(movement.currency)
    expect(view.concept).toBe(movement.concept)
    expect(view.description).toBe(movement.description)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = movement.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(movement.id)
    expect(view.from).toBe(movement.from)
    expect(view.to).toBe(movement.to)
    expect(view.amount).toBe(movement.amount)
    expect(view.currency).toBe(movement.currency)
    expect(view.concept).toBe(movement.concept)
    expect(view.description).toBe(movement.description)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
