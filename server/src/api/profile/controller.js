import { success, notFound } from '../../services/response/'
import { Profile } from '.'
import { Account } from '../account/';
import { Adventure } from '../adventure/';

export const create = ({ bodymen: { body }, user }, res, next) => {
  body.publications = []
  body.reviews = []
  body.documents = []
  body.skills = []
  body.impulses = []
  body.investments = []
  body.jobs = []
  body.products = []
  body.firstForm = true

  body.username = user.name;

  var currencies = ["ARS", "USD"];

  Profile.create(body)
    .then((profile) => {
      profile.view(true)
      _recursiveAccountCreation(0, currencies, user.id, function(data){
        res.send({});
    })
  })
}

function _recursiveAccountCreation(index, currencies, owner, callback){
  if (currencies.length){
    var newAccount = new Account();
    newAccount.owner = owner;
    newAccount.currency = currencies[index];
    newAccount.amount = 0;
    newAccount.save(function(err, data){
      if (err){
        throw err
      }
      else{
        if (data){
          index++;
          if (index < currencies.length)
            _recursiveAccountCreation(index, currencies, owner, callback);
          else
            callback && callback(data);
        }
      }
    })
  }
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Profile.find(query, select, cursor)
    .then((profiles) => profiles.map((profile) => profile.view()))
    .then(success(res))
    .catch(next)

export const byUsername = ({ params }, res, next) =>
  Profile.findOne({ username: params.username })
    .then(notFound(res))
    .then((profile) => profile ? profile.view() : null)
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Profile.findById(params.id)
    .then(notFound(res))
    .then((profile) => profile ? profile.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) => {
  function clean(obj) {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj;
  }
  if(body.publications && body.publications.length > 0) {
    body.publications[body.publications.length - 1].createdAt = new Date();
    body.publications[body.publications.length - 1].updatedAt = new Date();
  }

  Profile.findById(params.id)
    .then(notFound(res))
    .then((profile) => profile ? Object.assign(profile, clean(body)).save() : null)
    .then((profile) => {
      profile.rewards.forEach(item => {
        Adventure.findById(item.adventure.id)
        .then(notFound(res))
        .then((adventure) => adventure ? Object.assign(adventure, clean(body)).save() : null)
        .then((adventure) => {
          adventure.delivery.forEach((advDeliveryItem, index) => {
          if (advDeliveryItem.invop === item.invop) {
            adventure.delivery[index].status = item.status
          }
          })
          adventure.save()
          return adventure
        })
      })

      return profile ? profile.view(true) : null
    })
    .then(success(res))
    .catch(next)
}


export const destroy = ({ params }, res, next) =>
  Profile.findById(params.id)
    .then(notFound(res))
    .then((profile) => profile ? profile.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
