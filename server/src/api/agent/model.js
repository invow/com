import mongoose, { Schema } from 'mongoose'

const agentSchema = new Schema({
  seniority: {
    type: String
  },
  fullName: {
    type: String
  },
  identification: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: String
  },
  cv: {
    type: String
  },
  agreementAccepted: {
    type: Boolean
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

agentSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      seniority: this.seniority,
      fullName: this.fullName,
      identification: this.identification,
      email: this.email,
      phone: this.phone,
      cv: this.cv,
      agreementAccepted: this.agreementAccepted,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Agent', agentSchema)

export const schema = model.schema
export default model
