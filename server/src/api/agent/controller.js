import { success, notFound } from '../../services/response/'
import { Agent } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Agent.create(body)
    .then((agent) => agent.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Agent.find(query, select, cursor)
    .then((agents) => agents.map((agent) => agent.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Agent.findById(params.id)
    .then(notFound(res))
    .then((agent) => agent ? agent.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Agent.findById(params.id)
    .then(notFound(res))
    .then((agent) => agent ? Object.assign(agent, body).save() : null)
    .then((agent) => agent ? agent.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Agent.findById(params.id)
    .then(notFound(res))
    .then((agent) => agent ? agent.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
