import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update } from './controller'
import { schema } from './model'
export Price, { schema } from './model'

const router = new Router()
const { product, type, value, discount, includes, user } = schema.tree

/**
 * @api {post} /prices Create price
 * @apiName CreatePrice
 * @apiGroup Price
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam type Price's type.
 * @apiParam value Price's value.
 * @apiParam user Price's user.
 * @apiSuccess {Object} price Price's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Price not found.
 * @apiError 401 master access only.
 */
router.post('/',
  master(),
  body({ product, type, value, discount, includes: [Object], user }),
  create)

/**
 * @api {get} /prices Retrieve prices
 * @apiName RetrievePrices
 * @apiGroup Price
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} prices List of prices.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /prices/:id Retrieve price
 * @apiName RetrievePrice
 * @apiGroup Price
 * @apiSuccess {Object} price Price's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Price not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /prices/:id Update price
 * @apiName UpdatePrice
 * @apiGroup Price
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam type Price's type.
 * @apiParam value Price's value.
 * @apiParam user Price's user.
 * @apiSuccess {Object} price Price's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Price not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  body({ product, type, value, discount, includes: [Object], user }),
  update)

export default router
