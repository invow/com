import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export About, { schema } from './model'

const router = new Router()
const { title, content, video } = schema.tree

/**
 * @api {post} /abouts Create about
 * @apiName CreateAbout
 * @apiGroup About
 * @apiParam title About's title.
 * @apiParam content About's content.
 * @apiParam video About's video.
 * @apiSuccess {Object} about About's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 About not found.
 */
router.post('/',
  body({ title, content, video }),
  create)

/**
 * @api {get} /abouts Retrieve abouts
 * @apiName RetrieveAbouts
 * @apiGroup About
 * @apiUse listParams
 * @apiSuccess {Object[]} abouts List of abouts.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /abouts/:id Retrieve about
 * @apiName RetrieveAbout
 * @apiGroup About
 * @apiSuccess {Object} about About's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 About not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /abouts/:id Update about
 * @apiName UpdateAbout
 * @apiGroup About
 * @apiParam title About's title.
 * @apiParam content About's content.
 * @apiParam video About's video.
 * @apiSuccess {Object} about About's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 About not found.
 */
router.put('/:id',
  body({ title, content, video }),
  update)

/**
 * @api {delete} /abouts/:id Delete about
 * @apiName DeleteAbout
 * @apiGroup About
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 About not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
