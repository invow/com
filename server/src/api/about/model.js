import mongoose, { Schema } from 'mongoose'

const aboutSchema = new Schema({
  title: {
    type: String
  },
  content: {
    type: String
  },
  video: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

aboutSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      title: this.title,
      content: this.content,
      video: this.video,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('About', aboutSchema)

export const schema = model.schema
export default model
