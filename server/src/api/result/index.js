import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Result, { schema } from './model'

const router = new Router()
const { name, invcode, slug, image, blurb, days } = schema.tree

/**
 * @api {post} /results Create result
 * @apiName CreateResult
 * @apiGroup Result
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam name Result's name.
 * @apiParam slug Result's slug.
 * @apiSuccess {Object} result Result's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Result not found.
 * @apiError 401 user access only.
 */
router.post('/',
  body({ name, invcode, slug, image, blurb, days }),
  create)

/**
 * @api {get} /results Retrieve results
 * @apiName RetrieveResults
 * @apiGroup Result
 * @apiUse listParams
 * @apiSuccess {Object[]} results List of results.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /results/:id Retrieve result
 * @apiName RetrieveResult
 * @apiGroup Result
 * @apiSuccess {Object} result Result's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Result not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /results/:id Update result
 * @apiName UpdateResult
 * @apiGroup Result
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam name Result's name.
 * @apiParam slug Result's slug.
 * @apiSuccess {Object} result Result's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Result not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  body({ name, invcode, slug, image, blurb, days }),
  update)

/**
 * @api {delete} /results/:id Delete result
 * @apiName DeleteResult
 * @apiGroup Result
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Result not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  destroy)

export default router
