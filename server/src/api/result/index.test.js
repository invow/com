import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Result } from '.'

const app = () => express(apiRoot, routes)

let userSession, anotherSession, result

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const anotherUser = await User.create({ email: 'b@b.com', password: '123456' })
  userSession = signSync(user.id)
  anotherSession = signSync(anotherUser.id)
  result = await Result.create({ user })
})

test('POST /results 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, title: 'test', link: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.title).toEqual('test')
  expect(body.link).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('POST /results 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /results 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /results/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${result.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(result.id)
})

test('GET /results/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /results/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${result.id}`)
    .send({ access_token: userSession, title: 'test', link: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(result.id)
  expect(body.title).toEqual('test')
  expect(body.link).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('PUT /results/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${result.id}`)
    .send({ access_token: anotherSession, title: 'test', link: 'test' })
  expect(status).toBe(401)
})

test('PUT /results/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${result.id}`)
  expect(status).toBe(401)
})

test('PUT /results/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: anotherSession, title: 'test', link: 'test' })
  expect(status).toBe(404)
})

test('DELETE /results/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${result.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /results/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${result.id}`)
    .send({ access_token: anotherSession })
  expect(status).toBe(401)
})

test('DELETE /results/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${result.id}`)
  expect(status).toBe(401)
})

test('DELETE /results/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: anotherSession })
  expect(status).toBe(404)
})
