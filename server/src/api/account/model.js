import mongoose, { Schema } from 'mongoose'

const accountSchema = new Schema({
  type: {
    type: String
  },
  owner: {
    type: String
  },
  amount: {
    type: Number
  },
  currency: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

accountSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      type: this.type,
      owner: this.owner,
      amount: this.amount,
      currency: this.currency,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Account', accountSchema)

export const schema = model.schema
export default model
