import { Account } from '.'

let account

beforeEach(async () => {
  account = await Account.create({ type: 'test', owner: 'test', amount: 'test', currency: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = account.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(account.id)
    expect(view.type).toBe(account.type)
    expect(view.owner).toBe(account.owner)
    expect(view.amount).toBe(account.amount)
    expect(view.currency).toBe(account.currency)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = account.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(account.id)
    expect(view.type).toBe(account.type)
    expect(view.owner).toBe(account.owner)
    expect(view.amount).toBe(account.amount)
    expect(view.currency).toBe(account.currency)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
