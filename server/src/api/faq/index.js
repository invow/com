import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Faq, { schema } from './model'

const router = new Router()
const { title, content } = schema.tree

/**
 * @api {post} /faqs Create faq
 * @apiName CreateFaq
 * @apiGroup Faq
 * @apiParam title Faq's title.
 * @apiParam content Faq's content.
 * @apiSuccess {Object} faq Faq's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Faq not found.
 */
router.post('/',
  body({ title, content }),
  create)

/**
 * @api {get} /faqs Retrieve faqs
 * @apiName RetrieveFaqs
 * @apiGroup Faq
 * @apiUse listParams
 * @apiSuccess {Object[]} faqs List of faqs.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /faqs/:id Retrieve faq
 * @apiName RetrieveFaq
 * @apiGroup Faq
 * @apiSuccess {Object} faq Faq's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Faq not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /faqs/:id Update faq
 * @apiName UpdateFaq
 * @apiGroup Faq
 * @apiParam title Faq's title.
 * @apiParam content Faq's content.
 * @apiSuccess {Object} faq Faq's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Faq not found.
 */
router.put('/:id',
  body({ title, content }),
  update)

/**
 * @api {delete} /faqs/:id Delete faq
 * @apiName DeleteFaq
 * @apiGroup Faq
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Faq not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
