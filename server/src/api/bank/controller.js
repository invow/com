import { success, notFound } from '../../services/response/'
import { Bank } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Bank.create(body)
    .then((bank) => bank.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Bank.find(query, select, cursor)
    .then((banks) => banks.map((bank) => bank.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Bank.findById(params.id)
    .then(notFound(res))
    .then((bank) => bank ? bank.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Bank.findById(params.id)
    .then(notFound(res))
    .then((bank) => bank ? Object.assign(bank, body).save() : null)
    .then((bank) => bank ? bank.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Bank.findById(params.id)
    .then(notFound(res))
    .then((bank) => bank ? bank.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
