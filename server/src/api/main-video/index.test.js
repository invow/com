import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { MainVideo } from '.'

const app = () => express(apiRoot, routes)

let mainVideo

beforeEach(async () => {
  mainVideo = await MainVideo.create({})
})

test('POST /main-videos 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ title: 'test', path: 'test', user: 'test', slug: 'test', time: 'test', timeStart: 'test', dateStart: 'test', link: 'test', adventure: 'test', price: 'test', active: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.title).toEqual('test')
  expect(body.path).toEqual('test')
  expect(body.user).toEqual('test')
  expect(body.slug).toEqual('test')
  expect(body.time).toEqual('test')
  expect(body.timeStart).toEqual('test')
  expect(body.dateStart).toEqual('test')
  expect(body.link).toEqual('test')
  expect(body.adventure).toEqual('test')
  expect(body.price).toEqual('test')
  expect(body.active).toEqual('test')
})

test('GET /main-videos 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /main-videos 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /main-videos/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${mainVideo.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(mainVideo.id)
})

test('GET /main-videos/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /main-videos/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${mainVideo.id}`)
    .send({ access_token: masterKey, title: 'test', path: 'test', user: 'test', slug: 'test', time: 'test', timeStart: 'test', dateStart: 'test', link: 'test', adventure: 'test', price: 'test', active: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(mainVideo.id)
  expect(body.title).toEqual('test')
  expect(body.path).toEqual('test')
  expect(body.user).toEqual('test')
  expect(body.slug).toEqual('test')
  expect(body.time).toEqual('test')
  expect(body.timeStart).toEqual('test')
  expect(body.dateStart).toEqual('test')
  expect(body.link).toEqual('test')
  expect(body.adventure).toEqual('test')
  expect(body.price).toEqual('test')
  expect(body.active).toEqual('test')
})

test('PUT /main-videos/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${mainVideo.id}`)
  expect(status).toBe(401)
})

test('PUT /main-videos/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, title: 'test', path: 'test', user: 'test', slug: 'test', time: 'test', timeStart: 'test', dateStart: 'test', link: 'test', adventure: 'test', price: 'test', active: 'test' })
  expect(status).toBe(404)
})

test('DELETE /main-videos/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${mainVideo.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /main-videos/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${mainVideo.id}`)
  expect(status).toBe(401)
})

test('DELETE /main-videos/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
