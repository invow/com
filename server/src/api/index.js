import { Router } from 'express'
import user from './user'
import auth from './auth'
import adventure from './adventure'
import specialOffer from './special-offer'
import agreement from './agreement'
import profile from './profile'
import bank from './bank'
import source from './source'
import favorites from './favorites'
import category from './category'
import about from './about'
import faq from './faq'
import payment from './payment'
import mainVideo from './main-video'
import file from './file'
import fund from './fund'
import investment from './investment'
import account from './account'
import movement from './movement'
import checkout from './checkout'
import agent from './agent'
import subscription from './subscription'
import product from './product'
import result from './result'
import video from './video'
import operation from './operation'
import price from './price'

const router = new Router()

/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */
router.use('/users', user)
router.use('/auth', auth)
router.use('/adventures', adventure)
router.use('/image/thumb/:file', (req, res) => {
  const filesUrl = __dirname.replace('src/api', 'uploads/images/thumb')
  console.log(filesUrl)
  res.sendFile(filesUrl + '/' + req.params.file)
})
router.use('/image/:file', (req, res) => {
  const filesUrl = __dirname.replace('src/api', 'uploads/images')
  console.log(filesUrl)
  res.sendFile(filesUrl + '/' + req.params.file)
})
router.use('/docs/:file', (req, res) => {
  const filesUrl = __dirname.replace('src/api', 'uploads/docs')
  console.log(filesUrl)
  res.sendFile(filesUrl + '/' + req.params.file)
})

router.use('/special-offers', specialOffer)
router.use('/agreements', agreement)
router.use('/profiles', profile)
router.use('/banks', bank)
router.use('/sources', source)
router.use('/favorites', favorites)
router.use('/categories', category)
router.use('/abouts', about)
router.use('/faqs', faq)
router.use('/payments', payment)
router.use('/main-videos', mainVideo)
router.use('/files', file)
router.use('/funds', fund)
router.use('/investments', investment)
router.use('/accounts', account)
router.use('/movements', movement)
router.use('/checkouts', checkout)
router.use('/agents', agent)
router.use('/subscriptions', subscription)
router.use('/products', product)
router.use('/results', result)
router.use('/videos', video)
router.use('/operations', operation)
router.use('/prices', price)

export default router
