const fs = require('fs')
const RANGE = require('./range')

const FILE = {
    get: (headers, res) => {
        console.log('here')
        const path = 'assets/sample.mp4'
        const stat = fs.statSync(path)
        const fileSize = stat.size
        const range = headers.range
        RANGE.inRange(range, path, fileSize, res, fs)
    }
}

export default FILE

