import { Video } from '.'

let video

beforeEach(async () => {
  video = await Video.create({ name: 'test', file: 'test', duration: 'test', current: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = video.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(video.id)
    expect(view.name).toBe(video.name)
    expect(view.file).toBe(video.file)
    expect(view.duration).toBe(video.duration)
    expect(view.current).toBe(video.current)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = video.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(video.id)
    expect(view.name).toBe(video.name)
    expect(view.file).toBe(video.file)
    expect(view.duration).toBe(video.duration)
    expect(view.current).toBe(video.current)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
