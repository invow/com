import { success, notFound } from '../../services/response/'
import { Agreement } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Agreement.create(body)
    .then((agreement) => agreement.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Agreement.find(query, select, cursor)
    .then((agreements) => agreements.map((agreement) => agreement.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Agreement.findById(params.id)
    .then(notFound(res))
    .then((agreement) => agreement ? agreement.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Agreement.findById(params.id)
    .then(notFound(res))
    .then((agreement) => agreement ? Object.assign(agreement, body).save() : null)
    .then((agreement) => agreement ? agreement.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Agreement.findById(params.id)
    .then(notFound(res))
    .then((agreement) => agreement ? agreement.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
