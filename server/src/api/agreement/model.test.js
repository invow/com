import { Agreement } from '.'

let agreement

beforeEach(async () => {
  agreement = await Agreement.create({ title: 'test', amount: 'test', description: 'test', adventure: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = agreement.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(agreement.id)
    expect(view.title).toBe(agreement.title)
    expect(view.amount).toBe(agreement.amount)
    expect(view.description).toBe(agreement.description)
    expect(view.adventure).toBe(agreement.adventure)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = agreement.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(agreement.id)
    expect(view.title).toBe(agreement.title)
    expect(view.amount).toBe(agreement.amount)
    expect(view.description).toBe(agreement.description)
    expect(view.adventure).toBe(agreement.adventure)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
