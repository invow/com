import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Fund } from '.'

const app = () => express(apiRoot, routes)

let fund

beforeEach(async () => {
  fund = await Fund.create({})
})

test('POST /funds 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, concept: 'test', detail: 'test', input: 'test', output: 'test', amount: 'test', user: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.concept).toEqual('test')
  expect(body.detail).toEqual('test')
  expect(body.input).toEqual('test')
  expect(body.output).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.user).toEqual('test')
})

test('POST /funds 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /funds 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /funds 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /funds/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${fund.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(fund.id)
})

test('GET /funds/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${fund.id}`)
  expect(status).toBe(401)
})

test('GET /funds/:id 404 (master)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})

test('PUT /funds/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${fund.id}`)
    .send({ access_token: masterKey, concept: 'test', detail: 'test', input: 'test', output: 'test', amount: 'test', user: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(fund.id)
  expect(body.concept).toEqual('test')
  expect(body.detail).toEqual('test')
  expect(body.input).toEqual('test')
  expect(body.output).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.user).toEqual('test')
})

test('PUT /funds/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${fund.id}`)
  expect(status).toBe(401)
})

test('PUT /funds/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, concept: 'test', detail: 'test', input: 'test', output: 'test', amount: 'test', user: 'test' })
  expect(status).toBe(404)
})

test('DELETE /funds/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${fund.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /funds/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${fund.id}`)
  expect(status).toBe(401)
})

test('DELETE /funds/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
