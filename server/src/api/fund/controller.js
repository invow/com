import { success, notFound } from '../../services/response/'
import { Fund } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Fund.create(body)
    .then((fund) => fund.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Fund.find(query, select, cursor)
    .then((funds) => funds.map((fund) => fund.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Fund.findById(params.id)
    .then(notFound(res))
    .then((fund) => fund ? fund.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Fund.findById(params.id)
    .then(notFound(res))
    .then((fund) => fund ? Object.assign(fund, body).save() : null)
    .then((fund) => fund ? fund.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Fund.findById(params.id)
    .then(notFound(res))
    .then((fund) => fund ? fund.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
