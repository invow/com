import mongoose, { Schema } from 'mongoose'

const fundSchema = new Schema({
  concept: {
    type: String
  },
  report: {
    type: String
  },
  ready: {
    type: Boolean
  },
  type: {
    type: String
  },
  amount: {
    type: Number
  },
  currency: {
    type: String
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

fundSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      concept: this.concept,
      report: this.report,
      ready: this.ready,
      type: this.type,
      amount: this.amount,
      currency: this.currency,
      user: this.user,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Fund', fundSchema)

export const schema = model.schema
export default model
