import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token, master } from '../../services/passport'
import { create, index, show, update, byInvop } from './controller'
import { schema } from './model'
export Operation, { schema } from './model'

const router = new Router()
const { status, invop, items, total } = schema.tree

/**
 * @api {post} /operations Create operation
 * @apiName CreateOperation
 * @apiGroup Operation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam status Operation's status.
 * @apiParam invop Operation's invop.
 * @apiParam items Operation's items.
 * @apiParam total Operation's total.
 * @apiSuccess {Object} operation Operation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Operation not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ status, invop, items, total }),
  create)

/**
 * @api {get} /operations Retrieve operations
 * @apiName RetrieveOperations
 * @apiGroup Operation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} operations List of operations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /operations/:id Retrieve operation
 * @apiName RetrieveOperation
 * @apiGroup Operation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} operation Operation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Operation not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

router.get('/invop/:invop',
byInvop)

/**
 * @api {put} /operations/:id Update operation
 * @apiName UpdateOperation
 * @apiGroup Operation
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam status Operation's status.
 * @apiParam invop Operation's invop.
 * @apiParam items Operation's items.
 * @apiParam total Operation's total.
 * @apiSuccess {Object} operation Operation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Operation not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  body({ status, invop, items, total }),
  update)

export default router
