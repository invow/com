import { success, notFound } from '../../services/response/'
import { Operation } from '.'

export const create = ({ user, bodymen: { body } }, res, next) =>
  Operation.create({ ...body, user })
    .then((operation) => operation.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Operation.find(query, select, cursor)
    .populate('user')
    .then((operations) => operations.map((operation) => operation.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Operation.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((operation) => operation ? operation.view() : null)
    .then(success(res))
    .catch(next)

export const byInvop = ({ params }, res, next) => {
  Operation.findOne({ invop: params.invop })
    .then(notFound(res))
    .then((operation) => {
      return operation ? operation.view() : null
    })
    .then(success(res))
    .catch(next)

}
  // Adventure.findOne({ invcode: params.invcode })
  //   .then(notFound(res))
  //   .then((adventure) => adventure ? adventure.view() : null)
  //   .then(success(res))
  //   .catch(next)
  

export const update = ({ bodymen: { body }, params }, res, next) =>
  Operation.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((operation) => operation ? Object.assign(operation, body).save() : null)
    .then((operation) => operation ? operation.view(true) : null)
    .then(success(res))
    .catch(next)
