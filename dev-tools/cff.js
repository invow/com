/**
 * @Checkout Dev tool
 */

/**
 * @FORM elements
 */
let form = [
    {
        id: 'docType',
        value: 'DNI'
    },
    {
        id: 'docNumber',
        value: '12999999'
    },
    {
        id: 'cardExpirationMonth',
        value: '11'
    },
    {
        id: 'cardExpirationYear',
        value: '2025'
    }
];

/**
 * @Cards
 */
let Cards = 
{
    Visa: { 
        id: 'cardNumber',
        value: '4509953566233704'
    },
    Mastercard: {
        id: 'cardNumber',
        value: '5031755734530604'
    },
    SecCode: {
        id: 'securityCode',
        value: '123'
    },
    AmexSecCode: {
        id: 'securityCode',
        value: '1234'
    },
    Amex:{
        id: 'cardNumber',
        value: '371180303257522'
    }
};

/**
 * Init
 * @Map the form and pass fill method as a @param
 */
const init = () => {
    //start
    log('start');
    
    //add a credit card
    log('cc');
    form.push(Cards.Visa);
    form.push(Cards.SecCode);

    //map the form
    log('map')
    form.map(fill);

    //force submit button enabled
    log('button');
    let btn = $( "button[type='submit'].btn.btn-primary" );
    btn.prop('disabled', false);

    //end
    log('end');
};

const log = (type) => {
    switch(type) {
        case 'start':
        console.log(' ::: CFF ::: ');
        break;
        case 'cc':
        console.log('CFF ::: Adding a credit card ::: ');
        break;
        case 'button':
        console.log(' CFF ::: Enabling submit button ::: ');
        break;
        case 'map':
        console.log(' CFF ::: Mapping ::: ');
        break;
        case 'end': 
        console.log(' ::: /CFF :::');
        break;
    }
};

/**
 * Fill
 * @Get the element and @assign the value
 * @param {*} item 
 */
const fill = (item) => {
    let element = document.getElementById(item.id);
    let event = new Event('change');
    element.value = item.value;
    element.dispatchEvent(event);
};

init();